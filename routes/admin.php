<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::post('login', 'LoginController@login');

Route::group(['middleware' => 'auth.admin'], function () {

    Route::get('clear_cache_page', 'ClearCachePageController@index');

    Route::resource('settings', 'SettingDefaultController');
    Route::resource('embed_codes', 'SettingEmbedCodeController');

    Route::resource('menus', 'SettingMenuController');
    Route::get('menu_types', 'SettingMenuTypeController@index');
    Route::get('menu_supports', 'SettingMenuTypeController@index');
    Route::resource('menu_positions', 'SettingMenuPositionController');

    Route::resource('banner_groups', 'ContentBannerGroupController');
    Route::resource('banners', 'ContentBannerController');
    Route::post('banners/{id}', 'ContentBannerController@update');
    Route::put('banners/{id}/priority', 'ContentBannerController@update_priority');

    Route::resource('comments', 'ContentCommentController');
    Route::group(['prefix' => 'comments/{id}'], function () {
        Route::post('enable', 'ContentCommentController@enable');
        Route::post('disable', 'ContentCommentController@disable');
    });

    Route::resource('companies', 'ContentCompanyController');
    Route::resource('labels', 'SettingLabelController');
    Route::resource('structured_datas', 'StructuredDataController');

    Route::resource('company_branches', 'ContentCompanyBranchController');
    Route::post('company_branches/{id}', 'ContentCompanyBranchController@update');

    Route::resource('partners', 'ContentPartnerController');
    Route::post('partners/{id}', 'ContentPartnerController@update');


    Route::resource('staffs', 'ContentStaffController');
    Route::post('staffs/{id}', 'ContentStaffController@update');

    Route::resource('galleries', 'ContentGalleryController');
    Route::get('galleries/{id}/images', 'ContentGalleryController@index_images');
    Route::resource('photos', 'ContentPhotoController');
    Route::post('photos/{id}', 'ContentPhotoController@update');

    Route::resource('post_categories', 'ContentPostCategoryController');
    Route::post('post_categories/{id}', 'ContentPostCategoryController@update');
    Route::group(['prefix' => 'post_categories/{id}'], function () {
        Route::post('enable', 'ContentPostCategoryController@enable');
        Route::post('disable', 'ContentPostCategoryController@disable');
        Route::post('change_order', 'ContentPostCategoryController@change_order');
    });

    Route::resource('posts', 'ContentPostController');
    Route::post('posts/{id}', 'ContentPostController@update');
    Route::group(['prefix' => 'posts/{id}'], function () {
        Route::post('tags', 'ContentPostController@tags');
        Route::post('enable', 'ContentPostController@enable');
        Route::post('disable', 'ContentPostController@disable');
        Route::post('change_order', 'ContentPostController@change_order');
    });

    Route::resource('post_tags', 'ContentPostTagController');
    Route::resource('tags', 'ContentTagController');

    Route::resource('product_categories', 'ContentProductCategoryController');
    Route::post('product_categories/{id}', 'ContentProductCategoryController@update');
    Route::group(['prefix' => 'product_categories/{id}'], function () {
        Route::post('enable', 'ContentProductCategoryController@enable');
        Route::post('disable', 'ContentProductCategoryController@disable');
        Route::post('change_order', 'ContentProductCategoryController@change_order');
    });

    Route::resource('products', 'ContentProductController');
    Route::post('products/{id}', 'ContentProductController@update');
    Route::group(['prefix' => 'products/{id}'], function () {
        Route::post('enable', 'ContentProductController@enable');
        Route::post('disable', 'ContentProductController@disable');
        Route::post('change_order', 'ContentProductController@change_order');
    });

    Route::resource('pages', 'ContentPageController');
    Route::resource('seo_metas', 'SeoMetaController');
    Route::resource('widgets', 'SettingWidgetController');
    Route::resource('users', 'UserController');

    Route::resource('testimonials', 'ContentTestimonialController');
    Route::post('testimonials/{id}', 'ContentTestimonialController@update');
    Route::post('testimonials/{id}/change_order', 'ContentTestimonialController@change_order');

    Route::put('articles/{id}', 'ContentArticleController@update');
    Route::resource('languages', 'SettingLanguageController');

    Route::resource('product_types', 'ProductTypeController');
    Route::post('product_types/{id}', 'ProductTypeController@update');
    Route::group(['prefix' => 'product_types/{id}'], function () {
        Route::post('enable', 'ProductTypeController@enable');
        Route::post('disable', 'ProductTypeController@disable');
        Route::post('change_order', 'ProductTypeController@change_order');
    });

    Route::resource('orders', 'OrderController');

    Route::resource('customers', 'CustomerController');
    Route::get('get_category', 'ContentProductCategoryController@get_category');
});

