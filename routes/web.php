<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::fallback(function () {
    return response()->view('404-page', [], 404);
});

Route::get('/', 'HomeController@index');
Route::get('/lien-he', 'ContactController@index');
Route::post('/process_contact', 'ContactController@store');
Route::post('contact-message', 'ContactController@store');
//Route::get('/san-pham/{slug}/', 'ProductController@detail');
//Route::get('/danh-muc/{slug}/', 'ProductController@list');
Route::get('/tat-ca-san-pham', 'ProductController@index');
Route::get('/tim-kiem/', 'ProductController@search');
Route::get('/cart', 'CartController@index');
Route::get('/cart/{id}/delete', 'CartController@delete_item');
Route::post('/cart/add_item', 'CartController@add_item');
Route::get('/cart/{id}/down', 'CartController@down_item');
Route::get('/cart/{id}/up', 'CartController@up_item');
Route::get('/cart/{id}/update/{quantity}', 'CartController@update_quantity');
Route::get('/cart/checkout', 'CartController@get_checkout');
Route::post('/cart/checkout', 'CartController@checkout');
Route::get('/cart/{id}/add', 'CartController@add');
Route::get('/{slug}/', 'SlugController@index');
