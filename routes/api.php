<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('auth', 'AuthorController@index');
Route::middleware('auth.api')->group(function () {
    Route::resource('posts', 'PostController', ['only' => ['index', 'store', 'update', 'show', 'destroy']]);
    Route::post('posts/bulk', 'PostController@bulk');
    Route::group(['prefix' => 'posts/{id}'], function () {
        Route::put('disable', 'PostController@disable');
        Route::put('enable', 'PostController@enable');
        Route::resource('comments', 'CommentController', ['only' => ['index', 'store', 'update', 'destroy']]);
        Route::group(['prefix' => 'comments/{comment_id}'], function () {
            Route::put('disable', 'CommentController@disable');
            Route::put('enable', 'CommentController@enable');
        });
    });
    Route::resource('categories', 'CategoryController', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::post('categories/{id1}/transfer/{id2}', 'CategoryController@transfer');
    Route::resource('app-settings', 'AppSettingController', ['only' => ['index', 'store', 'update', 'destroy']]);
});