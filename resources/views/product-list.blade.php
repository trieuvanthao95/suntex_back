@extends('master')
@section('main')
    <div class="site-wrap page-inner">
        @include('block/header') 
        @include('block/box-page-title') 
        @include('block/breadcrumbs') 
        <div class="main-content">
            <div class="container">
                <div class="clearfix">
                    <div class="left-content">
                        @include('block/product-list') 
                    </div>
                    <div class="right-content">
                        @include('block/box-search-right') 
                        @include('block/box-product-category-right') 
                        @include('block/box-product-category-by-type-right') 
                    </div>
                </div>
            </div>
        </div>
        @include('block/footer') 
    </div>
@endsection
