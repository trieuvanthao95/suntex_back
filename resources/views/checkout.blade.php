@extends('master')
@section('main')
    <div class="site-wrap page-inner">
        @include('block/header')
        @include('block/box-page-title')
        @include('block/breadcrumbs')
        <div class="main-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-md-10">
                        @include('block/checkout')
                    </div>
                </div>
            </div>
        </div>
        @include('block/footer')
    </div>
@endsection
