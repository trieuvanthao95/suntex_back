@php
    $news = \App\Models\ContentPost::get_list(null,null,3);
@endphp
@if($news)
<div class="site-section">
    <div class="container">
        <h2 class="site-section-heading text-center text-uppercase mb-4">{{isset($language['news_title'])?$language['news_title']:'news_title'}}</h2>
        <div class="full-screen">
            <div class="owl-carousel owl-theme position-relative owl-small" id="news-index">
                @foreach($news as $p)
                <div class="item">
                    <div class="media-image">
                        @if($p['image'])
                        <a href="{{$p['full_path']}}">
                            <img data-src="{{$p['image_full_path']}}" alt="{{$p['alt']?$p['alt']:$p['name']}}" class="img-fluid lazy">
                        </a>
                        @endif
                        <div class="media-image-body">
                            <h3 class="h3-news" style="font-weight: 500">
                                <a href="{{$p['full_path']}}">{{$p['name']}}</a>
                            </h3>
                            <span class="d-block mb-3 date font-weight-light"><i class="fa fa-calendar"></i> {{date('d/m/Y',strtotime($p['created_at']))}}</span>
                            <div class="mb-3">{!! \App\Utils\HtmlUtil::extractShortText($p['article']['content'],'240','[...]')  !!}</div>
                            <div class="text-center">
                            <a href="{{$p['full_path']}}" class="btn btn-primary text-white px-4">{{isset($language['view_detail'])?$language['view_detail']:'detail'}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery('#news-index').owlCarousel({
        items: 3,
        margin: 20,
        autoplay: true,
        lazyLoad: true,
        autoplayTimeout: 4000,
        loop: true,
        dots: true,
        nav: false,
        smartSpeed: 450,
        navText: ['<span class="fa fa-angle-left">', '<span class="fa fa-angle-right">'],
        responsive: {
            0: {items: 1},
            767: {items: 1},
            768: {items: 2},
            1200: {items: 3}
        }
    });
</script>
@endif
