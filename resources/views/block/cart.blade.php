@if(count($content))
    <div id="ajax_loading_box" class="ajax-loading-box" style="display: none;"></div>
    <h2 class="margin-bottom-30">Giỏ hàng <span> ({{ \Cart::getTotalQuantity() }} {{isset($language['product'])?$language['product']:'product'}})</span></h2>
    @php
        $total_weight = 0;
        $total_price = \Cart::getTotal();
    @endphp
    <table class="table-cart">
        <thead>
        <tr>
            <th>{{isset($language['code'])?$language['code']:'code'}}</th>
            <th>{{isset($language['product_type'])?$language['product_type']:'product_type'}}</th>
            <th>{{isset($language['subtotal'])?$language['subtotal']:'subtotal'}}</th>
            <th>{{isset($language['quantity'])?$language['quantity']:'quantity'}}</th>
            <th>{{isset($language['total_price'])?$language['total_price']:'total_price'}}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($content as $product)
            <tr>
                <td>
                    <div class="d-flex align-items-md-center flex-cart">
                        <a class="img" href="{{$product['attributes']['full_path']}}" title="{{$product['name']}}">
                            <img class="img-responsive" src="{{$product['attributes']['image_full_path']}}"
                                 alt="{{$product['name']}}"/>
                        </a>
                        <div class="title">
                            <p>
                            <a class="smooth text-black"
                               href="{{$product['attributes']['full_path']}}">{{isset($language['code'])?$language['code']:'code'}}: {{$product['name']}}</a>
                            </p>
                            <div class="visible-xs">
                                @if($product['attributes']['type'])
                                    <dl class="dl-horizontal clearfix">
                                        <dt>{{isset($language['product_type'])?$language['product_type']:'product_type'}}:</dt>
                                        <dd>{{$product['attributes']['type']}}</dd>
                                    </dl>
                                @endif
                                <dl class="dl-horizontal">
                                    <dt>{{isset($language['price'])?$language['price']:'price'}}:</dt>
                                    <dd>{{\App\Utils\HtmlUtil::price_format($product['price'])}}</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>{{isset($language['quantity'])?$language['quantity']:'quantity'}}:</dt>
                                    <dd>
                                        <input class="form-control quantity" type="text" name="amount"
                                               data-rowid="{{$product['id']}}"
                                               min="0" value="{{$product['quantity']}}" style="display: inline-block;margin: 0">
                                    </dd>
                                </dl>
                                    <dl class="dl-horizontal">
                                        <dt>{{isset($language['total_price'])?$language['total_price']:'total_price'}}:</dt>
                                        <dd>{{\App\Utils\HtmlUtil::price_format($product['price'] * $product['quantity'])}}</dd>
                                    </dl>
                                <a class="remove-shop" href="/cart/{{ $product['id'] }}/delete"><i
                                        class="fa fa-remove"></i> {{isset($language['delete'])?$language['delete']:'delete'}}</a>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="hidden-xs">
                    @if($product['attributes']['type'])
                    {{$product['attributes']['type']}}
                        @endif
                </td>
                <td class="hidden-xs">
                    {{\App\Utils\HtmlUtil::price_format($product['price'])}}
                </td>
                <td class="hidden-xs">
                    <input class="form-control quantity" type="text" name="amount"
                             data-rowid="{{$product['id']}}"
                             min="0" value="{{$product['quantity']}}">
                </td>
                <td class="hidden-xs">
                    <strong>{{\App\Utils\HtmlUtil::price_format($product['price'] * $product['quantity'])}}</strong>
                </td>
                <td class="hidden-xs">
                    <a class="remove-shop" href="/cart/{{ $product['id'] }}/delete"><i
                            class="fa fa-remove"></i> {{isset($language['delete'])?$language['delete']:'delete'}}</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="total text-right">
        <p class="grand-total">
            <strong>{{isset($language['total'])?$language['total']:'total'}}:</strong>
            <big>{{\App\Utils\HtmlUtil::price_format($total_price)}}</big>
        </p>
        <div class="pay-ctrl">
            <a href="/tat-ca-san-pham" class="btn btn-large btn-secondary btn-checkout"><i
                    class="fa fa-reply"></i> {{isset($language['continue_buy'])?$language['continue_buy']:'continue_buy'}}
            </a>
            <a class="btn btn-large btn-success btn-checkout" href="/cart/checkout">
                <i class="fa fa fa-arrow-circle-o-right"></i> {{isset($language['send_order'])?$language['send_order']:'send_order'}}
            </a>
        </div>
    </div>
@else
    <div class="span16 main-content-grid">
        <div class="no-items">
            <p class="msg-no-items">{{isset($language['cart_empty'])?$language['cart_empty']:'cart_empty'}}</p>
            <div class="buttons-container wrap">
                <button type="button" class="btn btn-large btn-secondary btn-checkout"
                        onclick="location.href='/';return false;"><i class="fa fa-plus-circle"></i> {{isset($language['more_buy'])?$language['more_buy']:'more_buy'}}
                </button>
            </div>
        </div>
    </div>
@endif
<script type="text/javascript">
    $(document).ready(function ($) {
        $("input[name=amount]").change(function (e){
            var quantity = parseInt($(this).val());
            if (quantity > 0) {
                var row = $(this).attr("data-rowid");
                $.ajax({
                    type: "GET",
                    url: '/cart/' + row + '/update/' + quantity,
                    beforeSend: function () {
                    },
                    success: function (response) {
                        window.location.href = "/cart";
                    }
                });
            } else alert("Vui lòng nhập số lượng");
        })
    })
</script>
