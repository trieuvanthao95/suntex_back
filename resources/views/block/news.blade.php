@if($news)
    <div class="margin-bottom-30">
    @foreach($news as $n)
        <div class="item-news">
            @if($n['image'])
                <div class="row">
                    <div class="col-md-4">
                        <div class="left-news-list">
                            <a href="{{$n['full_path']}}">
                                <img data-src="{{$n['image_full_path']}}"
                                     alt="{{$n['name']}}" class="lazy"/>
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h3 class="h3-news">
                            <a class="text-black" href="{{$n['full_path']}}">
                                {{$n['name']}}
                            </a>
                        </h3>
                        <div class="date margin-bottom-10"><i
                                class="fa fa-calendar"></i> {{date('d/m/Y',strtotime($n['created_at']))}}</div>
                        <div class="f15 margin-bottom-10">
                            {!! \App\Utils\HtmlUtil::extractShortText($n['article']['content'],'180','[...]')  !!}
                        </div>
                        <div class="">
                            <a class="view-detail" href="{{$n['full_path']}}">{{isset($language['view_detail'])?$language['view_detail']:'view_detail'}} <i
                                    class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            @else
                <h3 class="h3-news">
                    <a class="text-black" href="{{$n['full_path']}}">
                        {{$n['name']}}
                    </a>
                </h3>
                <div class="date margin-bottom-10"><i
                        class="fa fa-calendar"></i> {{date('d/m/Y',strtotime($n['created_at']))}}</div>
                <div class="f15 margin-bottom-10">
                    {!! \App\Utils\HtmlUtil::extractShortText($n['article']['content'],'400','[...]')  !!}
                </div>
                <div class="">
                    <a class="view-detail" href="{{$n['full_path']}}">{{isset($language['view_detail'])?$language['view_detail']:'view_detail'}} <i class="fa fa-angle-right"></i></a>
                </div>
            @endif
        </div>
    @endforeach
    </div>
    <div class="pagination justify-content-center">{!! $news->links() !!}</div>
@endif
