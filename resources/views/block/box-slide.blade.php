@php
    $slides = \App\Models\ContentBannerGroup::get_code('slide');
@endphp
@if($slides)
    <div class="slide-one-item home-slider owl-carousel">
        @foreach($slides as $banner)
            <div class="site-blocks-cover inner-page overlay lazy" data-bg="url({{$banner['image_full_path']}}" style="background-image: url({{$banner['image_full_path_zip']}});">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-6 text-center aos-init aos-animate">
                            <h3 class="font-secondary  font-weight-bold text-uppercase">{!! $banner['summary'] !!}</h3>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif
