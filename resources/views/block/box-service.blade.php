@php
    $posts = \App\Models\ContentPost::get_post_by_label('service', 3);
    $label = \App\Models\SettingLabel::where('name', 'service')->first();
@endphp
@if($posts && $label)
<div class="site-section  box-service bg-grey">
    <div class="container">
        <div class="mb-5">
            <h2 class="site-section-heading text-uppercase text-center font-secondary">{{$label['title']}} </h2>
        </div>
        <div class="row border-responsive">
            @foreach($posts as $p)
                <div class="col-4  border-right ">
                <div class="text-center">
                    <a href="{{$p['full_path']}}">
                    <p class="margin-bottom-20">
                        @if($p['image'])
                            <img  data-src="{{$p['image_full_path']}}" alt="{{$p['alt']?$p['alt']:$p['name']}}" class="lazy"/>
                        @endif
                    </p>
                    </a>
                    <h3 class="h3-service font-weight-normal"><a href="{{$p['full_path']}}">{{$p['name']}}</a></h3>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif
