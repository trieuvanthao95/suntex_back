@if($type)
    <div id="myModalM" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Kiểu móc</h4>
                    <button type="button" class="close" data-dismiss="modal"><span>{{isset($language['close'])?$language['close']:'close'}}</span> x</button>
                </div>
                <div class="modal-body" style="overflow: auto;">
                        <div class="row row-m-5">
                            @foreach($type as $t)
                                <div class="col-md-3 margin-bottom-20 text-center col-6">
                                    <p>
                                        <a>
                                            <img alt="{{$t['alt']}}" class="img-full lazy" style="object-fit: cover;"
                                                 data-src="{{$t['image_full_path']}}"/>
                                        </a>
                                    </p>
                                    <h4 class="font-weight-normal" style="margin-bottom: 0">{{$t['name']}}</h4>
                                    <p class="tec">
                                        <a class="datmua clchon view-detail" code="{{$t['name']}}" href="javascript:">{{isset($language['choose'])?$language['choose']:'choose'}}</a>
                                    </p>
                                </div>
                            @endforeach
                    </div>
                    <button type="button" class="close" data-dismiss="modal"><span>{{isset($language['close'])?$language['close']:'close'}}</span> x</button>
                </div>
            </div>
        </div>
    </div>
@endif
