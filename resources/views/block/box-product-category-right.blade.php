@php
    $categories = \App\Models\ContentProductCategory::get_category_by_label('nhom_theo_chat_lieu', 3);
    $label = \App\Models\SettingLabel::where('name', 'nhom_theo_chat_lieu')->first();
@endphp
@if($categories && $label)
    <div class="sidebar-box">
        <div class="categories">
            <h3 class="text-uppercase">{{$label['title']}}</h3>
            <ul class="list-unstyled">
                @foreach($categories as $c)
                    <li>
                        <a href="{{$c['full_path']}}"><i class="fa fa-circle-o"></i> {{$c['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
