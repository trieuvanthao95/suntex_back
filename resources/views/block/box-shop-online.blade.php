@php
    $products = \App\Models\ContentProduct::get_product_by_label('shop_online', 10);
    $label = \App\Models\SettingLabel::where('name', 'shop_online')->first();
@endphp
@if($products && $label)
    <div class="site-section">
        <div class="container">
            <h2 class="site-section-heading text-uppercase text-center mb-4">
                <small class="font-secondary">{{$label['title']}}</small>
                @if($label['summary'])
                    {!!  $label['summary'] !!}
                @endif
            </h2>
            <div class="full-screen mb-4">
                <div class="owl-carousel owl-theme position-relative owl-small" id="shop-online">
                    @foreach($products as $p)
                        <div class="item text-center">
                            <a href="{{$p['full_path']}}">
                                @if($p['image'])
                                    <div class="position-relative cat-product margin-bottom-10">
                                        <img
                                             data-src="{{$p['image_full_path']}}" alt="{{$p['alt']?$p['alt']:$p['name']}}" class="lazy"/>
                                    </div>
                                @endif
                                <h3 class="h3-cat text-center font-secondary ">{{$p['name']}}</h3>
                            </a>
                            @if($p['price'])
                                <p class="margin-bottom-5">{{isset($language['price'])?$language['price']:'price'}}: <span
                                            class="price">{{App\Utils\HtmlUtil::price_format($p['price'])}}</span></p>
                            @endif
                            <a href="/cart/{{$p['id']}}/add/" class="view-detail"><i class="fa fa-shopping-cart"></i> {{isset($language['add_to_cart'])?$language['add_to_cart']:'add_to_cart'}}</a>
                        </div>
                    @endforeach
                </div>
                <script type="text/javascript">
                    jQuery('#shop-online').owlCarousel({
                        items: 5,
                        margin: 30,
                        autoplay: true,
                        lazyLoad: true,
                        autoplayTimeout: 4000,
                        loop: true,
                        dots: false,
                        nav: true,
                        smartSpeed: 450,
                        navText: ['<span class="fa fa-angle-left">', '<span class="fa fa-angle-right">'],
                        responsive: {
                            320: {items: 1},
                            450: {items: 2},
                            767: {items: 3},
                            992: {items: 4},
                            1200: {items: 5}
                        }
                    });
                </script>
            </div>
            <div class="text-center">
                <a href="/shop-online" class="text-pri text-black text-uppercase" style="text-decoration:underline "><i
                            class="fa fa-arrow-circle-right"></i> {{isset($language['view_more_product'])?$language['view_more_product']:'view_more_product'}}</a>
            </div>
        </div>
    </div>
@endif
