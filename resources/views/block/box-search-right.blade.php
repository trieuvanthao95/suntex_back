<div class="sidebar-box">
    <form action="/tim-kiem" class="search-form" method="get">
        <div class="form-group">
            <button type="submit" class="search-submit">
                <span class="icon fa fa-search"></span>
            </button>
            <input type="text" name="search" class="form-control" placeholder="{{isset($language['text_search'])?$language['text_search']:'text_search'}}">
        </div>
    </form>
</div>
