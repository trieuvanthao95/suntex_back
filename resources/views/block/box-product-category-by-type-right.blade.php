@php
    $categories = \App\Models\ContentProductCategory::get_category_by_label('nhom_theo_cong_nang', 8);
    $label = \App\Models\SettingLabel::where('name', 'nhom_theo_cong_nang')->first();
@endphp
@if($categories && $label)
<div class="sidebar-box">
    <div class="categories">
        <h3 class="text-uppercase">{{$label['title']}}</h3>
        <ul class="list-unstyled child-categories group" style="display: block;">
            @foreach($categories as $c)
            <li class="clearfix">
                <a href="{{$c['full_path']}}">
                    <img data-src="{{$c['image_full_path']}}" alt="{{$c['alt']}}" class="img-cat-small lazy"/>
                </a>
                <a href="{{$c['full_path']}}"  style="font-weight: 500">{{$c['name']}}</a>
            </li>
           @endforeach
        </ul>
    </div>
</div>
@endif
