@php
    use App\Utils\BreadcrumbsUtil;
    $breadcrumbs = BreadcrumbsUtil::getVars();
@endphp
@if($breadcrumbs)
    <div class="breadcrumbs">
            <div  class="container" itemscope itemtype="https://schema.org/BreadcrumbList">
                <div class="bread-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/" itemprop="item">
                        <span itemprop="name">Trang chủ</span>
                    </a>
                    <meta itemprop="position" content="1" />
                </div>
                @foreach($breadcrumbs as $k=>$b)
                    <div class="bread-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        @if($b['url'])
                            <a href="{{$b['url']}}" itemprop="item">
                                <span itemprop="name">{{$b['text']}}</span>
                            </a>
                        @else
                            <span itemprop="name"> {{$b['text']}}</span>
                        @endif
                        <meta itemprop="position" content="{{$k+2}}" />
                    </div>
                @endforeach
            </div>
    </div>
@endif

