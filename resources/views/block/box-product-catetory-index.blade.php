@php
    $categories = \App\Models\ContentProductCategory::get_category_by_label('nhom_theo_chat_lieu');
    $label = \App\Models\SettingLabel::where('name', 'nhom_theo_chat_lieu')->first();
@endphp
@if($categories && $label)
<div class="site-section">
    <div class="container">
        <div class="text-center margin-bottom-30">
            <h2 class="site-section-heading text-uppercase margin-bottom-20 text-center">{{$label['title']}}</h2>
            @if($label['summary'])
                <div>
                    {!!  $label['summary'] !!}
                </div>
            @endif
        </div>
        <div class="full-screen">
            <div class="owl-carousel owl-theme position-relative owl-small" id="cat-by-meta">
                @foreach($categories as $c)
                <div class="item text-center">
                    @if($c['image'])
                    <div class="position-relative cat-product margin-bottom-10">
                        <a href="{{$c['full_path']}}">
                            <img  data-src="{{$c['image_full_path']}}" alt="{{$c['alt']?$c['alt']:$c['name']}}" class="lazy"/>
                        </a>
                    </div>
                    @endif
                    <h2 class="h3-cat text-center font-secondary text-uppercase">
                        <a href="{{$c['full_path']}}">{{$c['name']}}</a>
                    </h2>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery('#cat-by-meta').owlCarousel({
        items: 3,
        margin: 20,
        autoplay: true,
        lazyLoad: true,
        autoplayTimeout: 4000,
        loop: true,
        dots: true,
        nav: true,
        smartSpeed: 450,
        navText: ['<span class="fa fa-angle-left">', '<span class="fa fa-angle-right">'],
        responsive: {
            0: {items: 1},
            767: {items: 1},
            768: {items: 2},
            1200: {items: 3}
        }
    });
</script>
@endif
