<div class="text-center margin-bottom-30">
    <h2 class="site-section-heading text-uppercase margin-bottom-40 text-center">{{$page->article->title}}</h2>
</div>
<form action="/process_contact" method="POST" id="frmContact">
    {{csrf_field()}}
    {!! $content !!}
</form>
