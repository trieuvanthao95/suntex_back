<h1 class="h1-detail">{{$page->article->title}}</h1>
<div class="main-entry">
    <div class="margin-bottom-30">
        {!! $page->article->content !!}
    </div>
    @include('block/share')
</div>
