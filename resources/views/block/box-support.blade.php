<div id="support_online-2" class="margin-bottom-30">
    <h3 class="widget-title">Hỗ trợ trực tuyến</h3>
    <img class="support-img" src="/images/3.png">
    <div class="gd_support_2">
        <div class="row-support">
            <p>Kỹ Thuật - Mr Chiến</p>
            <p><a href="tel:0936.686.019" class="phone">0936.686.019</a></p>
            <p><a href="mailto:kholanhvietnhat.vn@gmail.com" class="mail">kholanhvietnhat.vn@gmail.com</a></p>
        </div>
        <div class="row-support">
            <p>Kinh Doanh - Mr Tuân</p>
            <p><a href="tel:098.6363.108" class="phone">098.6363.108</a></p>
            <p><a href="mailto:kholanhvietnhat.vn@gmail.com" class="mail">kholanhvietnhat.vn@gmail.com</a></p>
        </div>
        <div class="row-support">
            <p>Kinh Doanh - Ms Oanh</p>
            <p><a href="tel:0243.99.234.99" class="phone">0243.99.234.99</a></p>
            <p><a href="mailto:kholanhvietnhat.vn@gmail.com" class="mail">kholanhvietnhat.vn@gmail.com</a></p>
        </div>
    </div>
</div>
