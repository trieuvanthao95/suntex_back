@if($products)
        <div class="full-screen">
            <div class="row row-m-5">
                @foreach($products as $p)
                    <div class="col-md-3 col-6 col-product margin-bottom-20 ">
                        <div class="text-center">
                            <a href="{{$p['full_path']}}">
                                <div class="position-relative cat-product margin-bottom-10">
                                    @if($p['image'])
                                        <img data-src="{{$p['image_full_path']}}" alt="{{$p['alt']}}" class="lazy"/>
                                    @endif
                                </div>
                                <h3 class="h3-cat text-center font-secondary ">{{$p['name']}}</h3>
                            </a>
                            <a href="{{$p['full_path']}}" class="view-detail"><i class="fa fa-angle-right"></i> {{isset($language['view_detail'])?$language['view_detail']:'view_detail'}}</a>
                        </div>
                    </div>
               @endforeach
            </div>
            <div class="pagination justify-content-center">{!! $products->links() !!}</div>
        </div>
@endif
