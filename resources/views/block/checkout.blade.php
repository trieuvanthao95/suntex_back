
    @if(count($content))
        <div class="margin-bottom-30">
            <h2 class="margin-bottom-30">{{isset($language['checkout'])?$language['checkout']:'checkout'}}</h2>
            @php
                $total_weight = 0;
                $total_price = \Cart::getTotal();
            @endphp
            <table class="table-cart">
                <thead>
                <tr>
                    <th>{{isset($language['code'])?$language['code']:'code'}}</th>
                    <th>{{isset($language['product_type'])?$language['product_type']:'product_type'}}</th>
                    <th>{{isset($language['subtotal'])?$language['subtotal']:'subtotal'}}</th>
                    <th>{{isset($language['quantity'])?$language['quantity']:'quantity'}}</th>
                    <th>{{isset($language['total_price'])?$language['total_price']:'total_price'}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($content as $product)
                    <tr>
                        <td>
                            <div class="d-flex align-items-md-center flex-cart">
                                <a class="img" href="{{$product['attributes']['full_path']}}" title="{{$product['name']}}">
                                    <img class="img-responsive" src="{{$product['attributes']['image_full_path']}}"
                                         alt="{{$product['name']}}"/>
                                </a>
                                <div class="title">
                                    <p>
                                    <a class="smooth text-black"
                                       href="{{$product['attributes']['full_path']}}">{{isset($language['code'])?$language['code']:'code'}}: {{$product['name']}}</a>
                                    </p>
                                    <div class="visible-xs">
                                        @if($product['attributes']['type'])
                                            <dl class="dl-horizontal clearfix">
                                                <dt>{{isset($language['product_type'])?$language['product_type']:'product_type'}}:</dt>
                                                <dd>{{$product['attributes']['type']}}</dd>
                                            </dl>
                                        @endif
                                        <dl class="dl-horizontal">
                                            <dt>{{isset($language['price'])?$language['price']:'price'}}:</dt>
                                            <dd>{{\App\Utils\HtmlUtil::price_format($product['price'])}}</dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>{{isset($language['quantity'])?$language['quantity']:'quantity'}}:</dt>
                                            <dd>
                                                {{$product['quantity']}}
                                            </dd>
                                        </dl>
                                            <dl class="dl-horizontal">
                                                <dt>{{isset($language['total_price'])?$language['total_price']:'total_price'}}:</dt>
                                                <dd>{{\App\Utils\HtmlUtil::price_format($product['price'] * $product['quantity'])}}</dd>
                                            </dl>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="hidden-xs">{{$product['attributes']['type']}}</td>

                        <td class="hidden-xs">
                            {{\App\Utils\HtmlUtil::price_format($product['price'])}}
                        </td>
                        <td class="hidden-xs">
                            <span class="visible-xs">x</span>  {{$product['quantity']}} <span class="visible-xs">=</span>
                        </td>
                        <td class="hidden-xs">
                            <strong>{{\App\Utils\HtmlUtil::price_format($product['price'] * $product['quantity'])}}</strong>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="total text-right">
                <p class="grand-total">
                    <strong>{{isset($language['total'])?$language['total']:'total'}}:</strong>
                    <big>{{\App\Utils\HtmlUtil::price_format($total_price)}}</big>
                </p>
            </div>
        </div>
        <div class="customer-info">
            <h4 class="h4-review margin-bottom-20">{{isset($language['information_buy'])?$language['information_buy']:'information_buy'}}</h4>
            <form id="frmcart" method="post" action="/cart/checkout">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <input type="hidden" name="total_weight" value="{{ $total_weight }}">
                <input type="hidden" name="total_price" value="{{ $total_price }}">
                <div class="row margin-bottom-20">
                    <div class="margin-bottom-10 col-md-6">
                        <input type="text" class="form-control f15" name="name" required
                               placeholder="{{isset($language['your_name'])?$language['your_name']:'your_name'}} (*)"/>
                    </div>
                    <div class="margin-bottom-10 col-md-6">
                        <input type="text" class="form-control f15" name="phone"
                               placeholder="{{isset($language['your_phone'])?$language['your_phone']:'your_phone'}} (*)" required/>
                    </div>
                    <div class="margin-bottom-10 col-md-6">
                        <input type="text" class="form-control f15" name="email" placeholder="{{isset($language['your_email'])?$language['your_email']:'your_email'}}"/>
                    </div>
                    <div class="margin-bottom-10 col-md-6">
                        <input type="text" class="form-control f15" name="address"
                               placeholder="{{isset($language['your_address'])?$language['your_address']:'your_address'}} (*)" required/>
                    </div>
                    <div class="margin-bottom-10 col-md-12">
                                            <textarea placeholder="{{isset($language['your_note'])?$language['your_note']:'your_note'}}" name="request"
                                                      class="form-control f15"></textarea>
                    </div>
                </div>
                <div class="margin-bottom-30"><em>{{isset($language['note_required'])?$language['note_required']:'note_required'}}</em></div>
                <div class="text-center">
                    <button type="submit" class="btn btn-large btn-success btn-checkout">
                        <i class="fa fa-envelope"></i> {{isset($language['send_order'])?$language['send_order']:'send_order'}}
                    </button>
                </div>
            </form>
        </div>
    @else
        <div class="span16 main-content-grid">
            <p class="no-items">{{isset($language['cart_empty'])?$language['cart_empty']:'cart_empty'}}</p>
            <div class="buttons-container wrap">
                <button type="button" class="btn btn-large btn-secondary btn-checkout"
                        onclick="location.href='/tat-ca-san-pham';return false;"><i class="fa fa-plus-circle"></i> {{isset($language['more_buy'])?$language['more_buy']:'more_buy'}}
                </button>
            </div>
        </div>
    @endif

