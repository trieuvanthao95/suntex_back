@if($products)
    <h1 class="h1-detail">Kết quả tìm kiếm sản phẩm</h1>
    <div class="mb-4">
        Có <strong>{{$products->total()}}</strong> sản phẩm phù hợp
    </div>
     <div class="full-screen">
        <div class="row mb-5">
            @foreach($products as $p)
                <div class="col-md-3 col-6 col-product margin-bottom-20 ">
                    <div class="text-center">
                        <a href="{{$p['full_path']}}">
                            <div class="position-relative cat-product margin-bottom-10">
                                @if($p['image'])
                                    <img  data-src="{{$p['image_full_path']}}" alt="{{$p['alt']}}" class="lazy"/>
                                @endif
                            </div>
                            <h3 class="h3-cat text-center font-secondary ">{{$p['name']}}</h3>
                        </a>
                        <a href="{{$p['full_path']}}" class="view-detail"><i class="fa fa-angle-right"></i> {{isset($language['view_detail'])?$language['view_detail']:'view_detail'}}</a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="pagination justify-content-center">{!! $products->appends(['search'=>$search])->links() !!}</div>
    </div>
@endif


