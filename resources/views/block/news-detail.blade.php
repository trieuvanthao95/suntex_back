@if($news)
    <h1 class="h1-detail">{{$news['name']}}</h1>
    <div class="date margin-bottom-10"><i class="fa fa-calendar"></i> {{date('d/m/Y',strtotime($news['created_at']))}}
    </div>
    <div class="main-entry">
        @if(strip_tags($news['summary']))
            <div class="margin-bottom-20">
                {!!  $news['summary'] !!}
            </div>
        @endif
        <div class="margin-bottom-30">
            {!!(\App\Utils\HtmlUtil::formatted_content($news['article']['content'])) !!}
        </div>
        <div class="margin-bottom-30">
            @include('block/share')
        </div>
    </div>
    @if(count($similar))
        <div class="similars">
            <h3 class="h3-similar">{{isset($language['news_similar'])?$language['news_similar']:'news_similar'}}</h3>
            <ul class="ul-right">
                @foreach($similar as $n)
                    <li>
                        <a href="{{$n['full_path']}}" class="text-black a-similar">{{$n['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
@endif
