@if($product)
<div class="margin-bottom-30">
    <div class="row">
        <div class="col-lg-6 product-img">
            <div class="thumbnails">
                <div class="product-additional margin-bottom-20">
                    <div class="pro-image">
                        @if($product['image'])
                            <img src="{{$product['image_full_path']}}" title="{{$product['name']}}" id="photo" alt="{{$product['alt']}}" />
                        @endif
                    </div>
                    @if($gallery)
                    <div id="additional-carousel" class="owl-carousel owl-theme clearfix">
                        @if($product['image'])
                            <div class="image-additional">
                                <a  title="{{$product['name']}}"  onmouseover="changeImage(this.name, this.title); return false; " name="{{$product['image_full_path']}}" class="cloud-zoom-gallery product-image">
                                    <img src="{{$product['image_full_path']}}" title="{{$product['name']}}" alt="{{$product['name']}}"/>
                                </a>
                            </div>
                        @endif
                        @foreach($gallery['images'] as $g)
                        <div class="image-additional">
                            <a title="{{$g['alt']?$g['alt']:$product['name']}}" onmouseover="changeImage(this.name, this.title); return false; " name="{{$g['image_full_path']}}" class="cloud-zoom-gallery product-image">
                                <img src="{{$g['image_full_path']}}" title="{{$g['alt']}}" alt="{{$g['alt']}}"/>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1 class="h1-detail-product text-pri clearfix">
                <span class="pull-left">{{isset($language['code_product'])?$language['code_product']:'code_product'}}:</span>
                <span class="pull-right">{{$product['name']}}</span>
            </h1>
            <div class="enuy">
                {!! $product['summary'] !!}
                <p>
                    <span id="kieuMoc" class="clearfix">
                        <span class="pull-left"><strong>{{isset($language['product_type'])?$language['product_type']:'product_type'}}:</strong></span>
                        <span class="fwb pull-right"><strong>{{isset($language['choose_product_type'])?$language['choose_product_type']:'choose_product_type'}}</strong> <i class="fa fa-caret-down"></i></span>
                    </span>
                </p>
            </div>
            <div class="form-book">
                <form action="/cart/add_item" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    <select id="chontype" name="type" class="form-control" style="display: none">
                        @foreach($type as $t)
                            <option value="{{$t['name']}}">{{$t['name']}}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="add-shopcart-detail cursor"><i class="fa fa-shopping-cart"></i> {{isset($language['text_order'])?$language['text_order']:'text_order'}}</button>
                </form>
             </div>
        </div>
    </div>
    @if(isset($widgets['box_support']['html']))
        <div class="note">
            <h5 class="text-uppercase">{{$widgets['box_support']['description']}}</h5>
            {!! $widgets['box_support']['html'] !!}
        </div>
    @endif
    @include('block/share')
</div>
@if($product_similar)
<div class="similar">
    <h3 class="h3-similar">
        <span>{{isset($language['product_similar'])?$language['product_similar']:'product_similar'}}</span>
    </h3>
    <div class="full-screen">
        <div class="owl-carousel owl-theme position-relative owl-small" id="product-similar">
            @foreach($product_similar as $p)
            <div class="item text-center">
                <a href="{{$p['full_path']}}">
                    @if($p['image'])
                    <div class="position-relative cat-product margin-bottom-10">
                        <img data-src="{{$p['image_full_path']}}" alt="{{$p['alt']}}" class="lazy"/>
                    </div>
                    @endif
                    <h3 class="h3-cat text-center font-secondary">{{$p['name']}}</h3>
                </a>
                @if($p['price'])
                    <p class="margin-bottom-5">Giá: <span class="price">{{App\Utils\StringUtil::price_format($p['price'])}}</span></p>
                @endif
                <a href="{{$p['full_path']}}" class="view-detail">{{isset($language['detail'])?$language['detail']:'detailchi '}} <i class="fa fa-angle-right"></i></a>
            </div>
            @endforeach
        </div>
        <script type="text/javascript">
            jQuery('#product-similar').owlCarousel({
                items: 4,
                margin: 30,
                autoplay: true,
                lazyLoad: true,
                autoplayTimeout: 4000,
                loop: true,
                dots: false,
                nav: true,
                smartSpeed: 450,
                navText: ['<span class="fa fa-angle-left">', '<span class="fa fa-angle-right">'],
                responsive: {
                    320: {items: 1},
                    450: {items: 2},
                    767: {items: 3},
                    992: {items: 4},
                    1200: {items: 4}
                }
            });
        </script>
    </div>
</div>
@endif
<script type="text/javascript">
    const direction = $('html').attr('dir');
    function changeImage(B, C) {
        jQuery("#photo").attr("src", B);
        jQuery("#photo").attr("alt", C);
        try {
            sendDataToOmnitureForThumbnail()
        } catch (A) {
        }
    }
    $('#additional-carousel').each(function () {
        $(this).addClass('owl-carousel owl-theme');
        const items = $(this).data('items') || 5;
        const sliderOptions = {
            loop: false,
            nav: true,
            navText: [''],
            dots: false,
            items: items,
            autoplay: false,
            responsiveRefreshRate: 200,
            margin: 5,
            navText: ['<span class="fa fa-angle-left">', '<span class="fa fa-angle-right">'],
            responsive: {
                0: {items: 2},
                320: {items: 4},
                426: {items: 4},
                768: {items: 5},
                992: {items: 5},
                1200: {items: 4},
                1441: {items: 4}
            }
        };
        if (direction == 'rtl') sliderOptions['rtl'] = true;
        $(this).owlCarousel(sliderOptions);
    });

</script>
@endif
