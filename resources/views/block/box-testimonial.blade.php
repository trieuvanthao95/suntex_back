<div class="box-bao-chi">
    <div class="container">
        <h2 class="h2-thanh-phan text-center">mọi người nói gì <br/>về sản phẩm của chúng tôi?</h2>
        <div class="owl-carousel owl-theme" id="testimonials">
            <div class="item text-center">
                <div class="top-test">
                    <img src="/images/cust1.jpg" class="img-cover"/>
                </div>
                <p class="name-cust">
                    Hà Tuấn Nam<small>Khách hàng</small>
                </p>
                <div class="desc-cust">
                    Rong nho rất ngon. Ăn giòn, ngậy. Cả nhà tôi đều thích
                </div>
            </div>
            <div class="item text-center">
                <div class="top-test">
                    <img src="/images/cust1.jpg" class="img-cover"/>
                </div>
                <p class="name-cust">
                    Hữu Tâm<small>Khách hàng</small>
                </p>
                <div class="desc-cust">
                    Tôi sẽ giới thiệu sản phẩm này đến nhiều người bạn của mình. Nó khá dinh dưỡng và bổ ích
                </div>
            </div>
            <div class="item text-center">
                <div class="top-test">
                    <img src="/images/cust1.jpg" class="img-cover"/>
                </div>
                <p class="name-cust">
                    Hữu Tâm<small>Khách hàng</small>
                </p>
                <div class="desc-cust">
                    Tôi chắc chắn sẽ đặt dài dài tại F99 về sản phẩm rong nho Nhật Bản. Nó khá ngon và tiện ích
                </div>
            </div>
            <div class="item text-center">
                <div class="top-test">
                    <img src="/images/cust1.jpg" class="img-cover"/>
                </div>
                <p class="name-cust">
                    Hữu Tâm<small>Khách hàng</small>
                </p>
                <div class="desc-cust">
                    Tôi chắc chắn sẽ đặt dài dài tại F99 về sản phẩm rong nho Nhật Bản. Nó khá ngon và tiện ích
                </div>
            </div>
            <div class="item text-center">
                <div class="top-test">
                    <img src="/images/cust1.jpg" class="img-cover"/>
                </div>
                <p class="name-cust">
                    Hữu Tâm<small>Khách hàng</small>
                </p>
                <div class="desc-cust">
                    Tôi chắc chắn sẽ đặt dài dài tại F99 về sản phẩm rong nho Nhật Bản. Nó khá ngon và tiện ích
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        jQuery('#testimonials').owlCarousel({
            autoplay: false,
            lazyLoad: true,
            autoplayTimeout: 4000,
            loop: false,
            margin: 30,
            responsiveClass: true,
            nav: false,
            dots: true,
            smartSpeed: 450,
            autoHeight: true,
            responsive: {
                0: {
                    items: 1,
                    autoHeight: true,
                },
                320: {
                    items: 2,
                },
                768: {
                    items: 3,
                },
                1024: {
                    items: 4
                }
            }
        });
    });
</script>
