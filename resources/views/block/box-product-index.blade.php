<div class="product_home clearfix">
    <div class="product_list clearfix">
        <h2 class="heading">
            <a class="title" href="/product-list.php">
                KHO LẠNH BẢO QUẢN
            </a>
            <a class="view-all" href="">
                Xem tất cả <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
        </h2>
        <div class="home-product">
            <div class="row col-margin-10">
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/a1.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Kho lạnh bảo quản dược phẩm
                                </a>
                            </h3>
                            <div class="text-center">
                                <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/a3.png"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Kho mát bảo quản nông sản từ 5m3 đến 500m3
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/a1.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Kho lạnh bảo quản dược phẩm
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/a3.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Cửa kho lạnh Việt Nhật
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product_list clearfix">
        <h2 class="heading">
            <a class="title" href="/product-list.php">
                Tháp giải nhiệt
            </a>
            <a class="view-all" href="">
                Xem tất cả <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
        </h2>
        <div class="home-product">
            <div class="row col-margin-10">
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/t1.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Tháp giải nhiệt nước
                                </a>
                            </h3>
                            <div class="text-center">
                                <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/t2.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    THÁP GIẢI NHIỆT 200RT
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/a1.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Kho lạnh bảo quản dược phẩm
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/t3.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    THÁP GIẢI NHIỆT CÔNG NGHIỆP 50RT
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product_list clearfix">
        <h2 class="heading">
            <a class="title" href="/product-list.php">
                Máy bơm
            </a>
            <a class="view-all" href="">
                Xem tất cả <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
        </h2>
        <div class="home-product">
            <div class="row col-margin-10">
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/b1.png"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Máy bơm FOERUN MKP 80
                                </a>
                            </h3>
                            <div class="text-center">
                                <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/b1.png"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Máy bơm FOERUN MKP 80
                                </a>
                            </h3>
                            <div class="text-center">
                                <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/b2.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Máy bơm FORERUN MCP 158
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/b3.png"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Máy bơm FORERUN MJSW/10M
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product_list clearfix">
        <h2 class="heading">
            <a class="title" href="/product-list.php">
                Panel
            </a>
            <a class="view-all" href="">
                Xem tất cả <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
        </h2>
        <div class="home-product">
            <div class="row col-margin-10">
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/p1.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Panel kho lạnh 1
                                </a>
                            </h3>
                            <div class="text-center">
                                <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/p2.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Panel kho lạnh
                                </a>
                            </h3>
                            <div class="text-center">
                                <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/p3.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Panel kho lạnh Việt Nhật
                                </a>
                            </h3>
                            <div class="text-center">
                            <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 col-product d-flex">
                    <div class="product-item d-flex flex-column">
                        <div class="top-product">
                            <a href="/product-detail.php">
                                <img src="/images/p3.jpg"  class="img-cover" alt=""/>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <div class="bottom-product d-flex flex-column">
                            <h3 class="h3-product">
                                <a class="text-black" href="/product-detail.php">
                                    Panel kho lạnh Việt Nhật
                                </a>
                            </h3>
                            <div class="text-center">
                                <a class="view-detail" href="/product-detail.php">Xem chi tiết <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
