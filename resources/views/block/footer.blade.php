<footer class="site-footer bg-dark">
    <div class="container">
        <p class="mb-4">
            @php
                $logo = \App\Models\ContentBannerGroup::get_code('logo-footer');
            @endphp
            @if($logo)
                <a href="{{$logo[0]['href']}}">
                    <img src="{{$logo[0]['image_full_path']}}"
                         alt="{{$logo[0]['alt']}}" width="230"/>
                </a>
            @endif
        </p>
        <div class="row">
            @if(isset($widgets['footer_address']['html']))
                <div class="col-md-5 col-footer">
                    <h4 class="footer-heading">{{$widgets['footer_address']['description']}}</h4>
                    {!! $widgets['footer_address']['html'] !!}
                </div>
            @endif
            @if(isset($menus['menu_footer']) && count($menus['menu_footer']))
                <div class="col-md-3 col-footer">
                    <h4 class="footer-heading"> {{isset($language['menu_footer'])?$language['menu_footer']:'menu_footer'}}</h4>
                    <ul class="list-unstyled">
                        @foreach($menus['menu_footer'] as $menu)
                            <li><a href="{{$menu['url']}}">{{$menu['name']}}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(isset($widgets['footer_follow_us']['html']))
                <div class="col-md-4 col-footer">
                    <h4 class="footer-heading">Follow us</h4>
                    {!! $widgets['footer_follow_us']['html'] !!}
                </div>
            @endif

        </div>
        <div class="text-center copyright">
            {{isset($language['copyright'])?$language['copyright']:'copyright'}}
        </div>
    </div>
</footer>
<div class="supports">
    <div class="support-box">
        <a class="button-icon messager-icon lazy" style="background-image: url(/images/widget_icon_messenger.svg)"
           href="{{isset($settings['facebook_chat'])?$settings['facebook_chat']:''}}" target="_blank" title="Chat ngay để nhận tư vấn" data-was-processed="true"><i></i></a>
    </div>

    <div class="support-box">
        <a title="Gọi ngay để được tư vấn!" class="button-icon phone-icon lazy"
           style="background-image:url(/images/widget_icon_click_to_call.svg)" href="tel:{{$settings['phone']}}" target="_blank" id="zalo"
           data-was-processed="true"><i></i></a>
    </div>
    <div class="support-box">
        <a title="Gửi thông tin liên hệ" class="button-icon email-icon lazy"
           style="background-image:url(/images/widget_icon_contact_form.svg)" href="mailto:{{$settings['master_email']}}"
           data-was-processed="true"><i></i></a>
    </div>
    <div id="dvBackToTop" class="backtotop">
        <i class="fa fa-angle-up"></i>
    </div>
</div>
@if(isset($settings['phone']))
<div id="callnow">
    <a href="tel:{{$settings['phone']}}" class="font-secondary">
        {{$settings['phone']}}
    </a>
</div>
@endif
<div class="popup_footer_support support_footer">
    <div class="popup_footer_support_content">
        <div class="popup_support_item">
            <a href="#" class="open_popup_footer_support">
                <i class="fa fa-bars"></i>
                <span>Follow us</span>
            </a>
            @if(isset($widgets['support_follow_us']['html']))
            <div class="popup_footer_content hidden">
               {!! $widgets['support_follow_us']['html'] !!}
                <span class="closer_popup_footer_suppor"><i class="fa fa-remove"></i> {{isset($language['close'])?$language['close']:'close'}} </span>
            </div>
            @endif
        </div>
        @if(isset($widgets['support_chat']['html']))
        <div class="popup_support_item">
            <a href="#" class="open_popup_footer_support" target="_blank">
                <i class="fa fa-comment-o"></i>
                <span>{{isset($language['messager'])?$language['messager']:'messager'}}</span>
            </a>
            <div class="popup_footer_content hidden">
                @if(($widgets['support_chat']['html']))
                    {!! $widgets['support_chat']['html'] !!}
                @endif
                <span class="closer_popup_footer_suppor"><i class="fa fa-remove"></i> {{isset($language['close'])?$language['close']:'close'}}</span>
            </div>
        </div>
        @endif
        @if(isset($widgets['support_hotline']['html']))
        <div class="popup_support_item">
            <a href="#" class="open_popup_footer_support">
                <i class="fa fa-phone"></i>
                <span>Hotline</span>
            </a>
            <div class="popup_footer_content hidden">
                @if(($widgets['support_hotline']['html']))
                    {!! $widgets['support_hotline']['html'] !!}
                @endif
                <span class="closer_popup_footer_suppor"></i> {{isset($language['close'])?$language['close']:'close'}} </span>
            </div>
        </div>
        @endif
        <div class="popup_support_item">
            <a href="mailto:{{$settings['master_email']}}">
                <i class="fa fa-envelope-o"></i>
                <span>Email</span>
            </a>
        </div>
        <div class="popup_support_item">
            <a href="/cart">
                <i class="fa fa-shopping-cart"></i>
                <span>{{isset($language['cart'])?$language['cart']:'cart'}}</span>
            </a>
        </div>
    </div>
</div>
