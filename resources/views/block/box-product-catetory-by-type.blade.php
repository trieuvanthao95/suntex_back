@php
    $categories = \App\Models\ContentProductCategory::get_category_by_label('nhom_theo_cong_nang', 8);
    $label = \App\Models\SettingLabel::where('name', 'nhom_theo_cong_nang')->first();
@endphp
@if($categories && $label)
    <div class="site-section">
        <div class="container">
            <div class="text-center margin-bottom-30">
                <h2 class="site-section-heading text-uppercase margin-bottom-20 text-center">{{$label['title']}}</h2>
                @if($label['summary'])
                <div>
                    {!!  $label['summary'] !!}
                </div>
                    @endif
            </div>
            <div class="full-screen">
                <div class="row cat-by-type">
                    @foreach($categories as $c)
                        <div class="col-md-3 col-6 margin-bottom-20">
                            <a href="{{$c['full_path']}}">
                                @if($c['image'])
                                    <div class="position-relative cat-product margin-bottom-10">
                                        <img  data-src="{{$c['image_full_path']}}" alt="{{$c['alt']?$c['alt']:$c['name']}}" class="lazy"/>
                                    </div>
                                @endif
                                <h3 class="h3-cat text-center font-secondary">{{$c['name']}}</h3>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
