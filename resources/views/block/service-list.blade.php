@if($news)
    @foreach($news as $n)
        <div class="item-service row">
            <div class="col-sm-2">
                <a href="{{$n['full_path']}}">
                    <img data-src="{{$n['image_full_path']}}"
                         alt="{{$n['name']}}" class="lazy"/>
                </a>
            </div>
            <div class="col-sm-10">
                <h3 class="h3-news font-weight-bold">
                    <a class="text-black" href="{{$n['full_path']}}">
                        {{$n['name']}}
                    </a>
                </h3>
                {!! $n['summary'] !!}
                <div class="">
                    <a class="view-detail" href="{{$n['full_path']}}">{{isset($language['view_detail'])?$language['view_detail']:'view_detail'}} <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    @endforeach
@endif
