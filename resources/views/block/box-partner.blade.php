@php
    $partners = \App\Models\ContentPartner::all()->toArray();
@endphp
@if($partners && count($partners))
    @php
        $partners = array_chunk($partners, 3);
    @endphp
<div class="site-section">
    <div class="container">
    <div class="text-center mb-5">
        <h2 class="site-section-heading text-uppercase text-center">{{isset($language['partner_title'])?$language['partner_title']:'partner_title'}}</h2>
         {{isset($language['partner_desc'])?$language['partner_desc']:'partner_desc'}}
    </div>
        <!-- cho thành hai hàng -->
    <div class="owl-carousel owl-theme" id="partner">
        @for($i = 0; $i < count($partners); $i++)
            <div class="item">
                @for($j = 0; $j < count($partners[$i]); $j++)
                    @if($partners[$i][$j]['image'])
                        <p>
                            <a target="_blank" href="{{$partners[$i][$j]['website']}}">
                            <img class="lazy" data-src="{{$partners[$i][$j]['image_full_path']}}" alt="{{$partners[$i][$j]['alt']}}" />
                            </a>
                        </p>
                    @endif
                @endfor
            </div>
        @endfor
    </div>
    <script type="text/javascript">
        jQuery('#partner').owlCarousel({
            items: 7,
            margin: 50,
            autoplay: true,
            lazyLoad: true,
            autoplayTimeout: 4000,
            loop: true,
            dots: true,
            smartSpeed: 450,
            responsive: {
                320: {items: 3},
                567: {items: 4},
                767: {items: 5},
                992: {items: 6},
                1200: {items: 7}
            }
        });
    </script>
    </div>
</div>
@endif
