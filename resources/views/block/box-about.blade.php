@php
    $banner_about = \App\Models\ContentBannerGroup::get_code('box_about');
@endphp
@if(isset($widgets['box_about']['html']))
    <div class="site-section  ">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="text-center">
                @if($banner_about)
                    <img data-src="{{$banner_about[0]['image_full_path']}}"
                         alt="{{$banner_about[0]['alt']}}" class="img-full lazy" />
                @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="left-about">
                    <div class="text-center">
                        <h2 class="site-section-heading text-uppercase  mb-4 text-center">{{$widgets['box_about']['description']}}</h2>
                    </div>
                    {!! $widgets['box_about']['html'] !!}
                </div>
            </div>
        </div>
    </div>
@endif
