@if($products && $categoryinfo)
    @if(strip_tags($categoryinfo['summary']))
        <div class="margin-bottom-20">
            {!! $categoryinfo['summary'] !!}
        </div>
    @endif
    <div class="full-screen">
        <div class="row row-m-5">
            @foreach($products as $p)
                <div class="col-md-3 col-6 col-product ">
                    <div class="text-center">
                        <a href="{{$p['full_path']}}">
                            <div class="position-relative cat-product margin-bottom-10">
                                @if($p['image'])
                                    <img src="{{$p['image_full_path_zip']}}" data-src="{{$p['image_full_path']}}"
                                         alt="{{$p['alt']?$p['alt']:$p['name']}}" class="lazy"/>
                                @endif
                            </div>
                            <h3 class="h3-cat text-center font-secondary ">{{$p['name']}}</h3>
                        </a>
                        @if($p['price'] && $categoryinfo['slug']=='shop-online')
                            <p class="margin-bottom-5">{{isset($language['price'])?$language['price']:'price'}}: <span class="price">{{App\Utils\StringUtil::price_format($p['price'])}}</span></p>
                        @endif
                        <a href="{{$p['full_path']}}" class="view-detail"><i class="fa fa-angle-right"></i> chi tiết</a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="pagination justify-content-center">{!! $products->links() !!}</div>
    </div>
@endif
