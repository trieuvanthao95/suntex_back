@php
    $logo = \App\Models\ContentBannerGroup::get_code('logo');
@endphp
<header id="header">
    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close">
                @if($logo)
                    <a href="{{$logo[0]['href']}}">
                        <img src="{{$logo[0]['image_full_path']}}"
                             alt="{{$logo[0]['alt']}}" width="200"/>
                    </a>
                @endif
            </div>
        </div>
        <div class="site-mobile-menu-body">
            <form role="search" method="get" class="form-search margin-bottom-20" action="/tim-kiem">
                <input type="text"  class="search-field" placeholder="{{isset($language['text_search'])?$language['text_search']:'text_search'}}" value="" name="search">
                <button type="submit" class="search-submit">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
            </form>
            <div class="menu-body"></div>
            <div>
                <span class="icon-close2 js-menu-toggle">{{isset($language['close'])?$language['close']:'close'}}</span>
            </div>
        </div>
    </div>
    <div class="site-navbar-wrap js-site-navbar bg-white">
        <div class="container">
            <div class="site-navbar bg-light">
                <div class="row align-items-center">
                    <div class="col-lg-8 col-md-4 col-3" id="header-nav">
                        @if(isset($menus['menu_head']) && count($menus['menu_head']) > 0)
                        <nav class="site-navigation" role="navigation">
                            <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3">
                                <a href="#" class="site-menu-toggle js-menu-toggle text-black active">
                                    <span class="icon-menu"></span>
                                </a>
                            </div>
                            <ul class="site-menu js-clone-nav d-none d-lg-block">
                                @foreach($menus['menu_head'] as $menu)
                                    <li class="{{isset($menu['children']) ? 'has-children' : ''}}">
                                        <a href="{{$menu['url']}}" class="font-secondary">{{$menu['name']}}</a>
                                        @isset($menu['children'])
                                            <ul class="dropdown arrow-top">
                                                @foreach($menu['children'] as $child)
                                                    <li><a href="{{$child['url']}}">{{$child['name']}}</a></li>
                                                @endforeach
                                            </ul>
                                        @endisset
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                        @endif
                    </div>
                    <div class="col-lg-2 col-md-4 col-6" id="header-logo">
                        @if($logo)
                            <a href="{{$logo[0]['href']}}">
                                <img src="{{$logo[0]['image_full_path']}}"
                                     alt="{{$logo[0]['alt']}}" class="logo"/>
                            </a>
                        @endif
                    </div>
                    <div class="col-lg-2 col-md-4 col-3" id="header-search">
                        <span class="search_button"><i class="fa fa-search"></i></span>
                        <form role="search" method="get" class="form-search search_toggle" action="/tim-kiem">
                            <input type="text"  class="search-field" placeholder="{{isset($language['text_search'])?$language['text_search']:'text_search'}}" value="" name="search">
                            <button type="submit" class="search-submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <form role="search" method="get" class="form-search search_toggle" action="/tim-kiem" id="frm-header-search">
                    <input type="text"  class="search-field" placeholder="{{isset($language['text_search'])?$language['text_search']:'text_search'}}" value="" name="search">
                    <button type="submit" class="search-submit">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</header>
