@extends('master')
@section('main')
    @include('block/box-slide')
    @include('block/breadcrumbs')
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 margin-bottom-20">
                    @include('block/faq')
                </div>
                <div class="col-lg-3">
                    @include('block/box-tour-category')
                    @include('block/box-tour-offer-right')
                    @include('block/box-guide-category-right')
                </div>
            </div>
        </div>
    </div>
@endsection
