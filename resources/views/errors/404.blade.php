@extends('master')
@section('main')
    <div id="content" class="site-content">
        <div class="container">
            @include('block/breadcrumbs')
            <div class="clearfix main-content">
                <h2 style="font-size: 40px">Error 404</h2>
                <div class="margin-bottom-30" style="font-size: 20px">
                    <div class="margin-bottom-10" style="font-size: 30px">
                        <i class="fa fa-frown-o" aria-hidden="true"></i> Sorry!
                    </div>
                    We looked everywhere, but couldn't find the page you requested.
                </div>
                <div class="margin-bottom-40">
                    <a class="btn btn-primary" href="/" style="margin-right: 20px"><i class="fa fa-reply"></i> BACK TO HOME</a>
                    <a class="btn btn-danger" href="/lien-he/"><i class="fa fa-envelope"></i> CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
@endsection
