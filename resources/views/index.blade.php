@extends('master')
@section('main')
<div class="site-wrap">
    @include('block/header')
    @include('block/box-slide')
    @include('block/box-product-catetory-index')
    @include('block/box-shop-online')
    @include('block/box-product-catetory-by-type')
    @include('block/box-service')
    @include('block/box-partner')
    @include('block/box-about')
    @include('block/box-news')
    @include('block/footer')
</div>
@endsection
