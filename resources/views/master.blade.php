<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <base href="{{env("APP_URL")}}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="content-language" content="vi">
    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,700|Work+Sans:300,400,500,600,700&display=swap" rel="stylesheet">

    <title>{{$meta['title']??$settings['meta_title']??'Suntex'}}</title>
    <meta name="description"
          content="{{$meta['description']??$settings['meta_description']??'Suntex'}}">
    <meta name="keywords"
          content="{{$meta['keywords']??''}}">
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title"
          content="{{$meta['title']??$settings['meta_title']??'Suntex'}}"/>
    <meta property="og:description"
          content="{{$meta['description']??$settings['meta_description']??'Suntex'}}"/>
    @if(isset($meta['image']))
        <meta property="og:image" content="{{$meta['image']}}"/>
    @endif
    @if(isset($meta['canonical']))
        <link rel="canonical" href="{{$meta['canonical']}}" />
    @endif
    <!--CSS-->
    <link rel="stylesheet" type="text/css" href="/build/app.css">
    <script language="javascript" src="/build/app-min.js"></script>
    @include('block/common')
    @php
        foreach ($php_codes as $code) {
           eval($code);
       }
    @endphp
    @foreach($header_embed_codes as $code)
        {!! $code !!}
    @endforeach
    @yield('extra_head')
</head>
<body>
@foreach($footer_embed_codes as $code)
    {!! $code !!}
@endforeach
@yield('main')
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
<script type="text/javascript">
    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
    });
    if (lazyLoadInstance) {
        lazyLoadInstance.update();
    }
</script>
</body>
</html>
