<!doctype html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WaytoMekong</title>
</head>
<body>
<p><strong>Dear (Ms/Mr)</strong> {{$name}} <{{$email}}></p>

<p>Thank you very much for your booking to WaytoMekong, We have well received your email with below details :</p>
<strong>Your full name: </strong>{{ $name }}<br/>
<strong>Your phone No: </strong>{{ $phone }}<br/>
<strong>Your Address: </strong>{{ $address }}<br/>

<strong>Your requests: </strong>{{ $content }}<br/>
We will get back to you as soon as possible
<br/><br/>
Best regards,<br/>
WaytoVietnam Travel Team
</body>
</html>
