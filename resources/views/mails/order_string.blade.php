<table width="100%" border="1" class="table_checkout"  style="font-family:Arial;border-collapse: collapse;border-spacing: 0;border-color: #CC9833;" cellpadding="0" cellspacing="0">
    <tr>
        <th style=" border-color: #CC9833;padding: 8px;">{{isset($language['code'])?$language['code']:'code'}}</th>
        <th style=" border-color: #CC9833;padding: 8px;">{{isset($language['product_type'])?$language['product_type']:'product_type'}}</th>
        <th style=" border-color: #CC9833;padding: 8px;">{{isset($language['subtotal'])?$language['subtotal']:'subtotal'}}</th>
        <th style=" border-color: #CC9833;padding: 8px;">{{isset($language['quantity'])?$language['quantity']:'quantity'}}</th>
        <th style=" border-color: #CC9833;padding: 8px;">{{isset($language['total_price'])?$language['total_price']:'total_price'}}</th>
    </tr>
    @foreach($content as $product)
        <tr>
            <td align="left" style=" border-color: #CC9833;padding: 8px;">
                {{$product['name']}}
            </td>
            <td align="left" style=" border-color: #CC9833;padding: 8px;">
                @if($product['attributes']['type'])
                    {{$product['attributes']['type']}}
                @endif
            </td>
            <td align="right" style=" border-color: #CC9833;padding: 8px;">
                {{\App\Utils\HtmlUtil::price_format($product['price'])}}
            </td>

            <td align="center" style=" border-color: #CC9833;padding: 8px;">
                {{$product['quantity']}}
            </td>

            <td align="right" style=" border-color: #CC9833;padding: 8px;">
                <strong>{{\App\Utils\HtmlUtil::price_format($product['price'] * $product['quantity'])}}</strong>
            </td>
        </tr>
    @endforeach
    <tr>
        <td colspan="4" align="right" style=" border-color: #CC9833;padding: 8px;"><strong>{{isset($language['total'])?$language['total']:'total'}}:</strong></td>
        <td align="right" style=" border-color: #CC9833;padding: 8px;">
            <span class="total" style="color: #ff0000;font-size: 16px;font-weight: bold;">
                {{\App\Utils\HtmlUtil::price_format($total)}}USD
            </span>
        </td>
    </tr>
    @if($more_request)
        <tr>
            <td colspan="4" align="right" style=" border-color: #CC9833;padding: 8px;">
               {{$more_request}}
            </td>
        </tr>
    @endif
</table>
