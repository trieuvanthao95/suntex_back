<!doctype html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$settings['master_name']}}</title>
</head>
<body>
<div style="padding: 40px 0;background: #FFFFF0;">
    <div style="max-width: 700px;margin: 0 auto;">
        {!! $email_content !!}
    </div>
</div>
</body>
</html>
