@extends('master')
@section('main')
    <div class="site-wrap page-inner">
        @include('block/header')
        @include('block/box-page-title')
        @include('block/breadcrumbs')
        <div class="main-content">
            <div class="container">
                  @include('block/cart')
            </div>
        </div>
        @include('block/footer')
    </div>
@endsection
