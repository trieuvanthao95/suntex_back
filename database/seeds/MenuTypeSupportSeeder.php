<?php

use Illuminate\Database\Seeder;

class MenuTypeSupportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new \App\Models\SettingMenuType();
        $new->name = 'Sub Page';
        $new->code = '$data = \App\Models\ContentPage::get();';
        $new->type = 'pages';
        $new->save();
        $new = new \App\Models\SettingMenuType();
        $new->name = 'Danh mục bài viết';
        $new->code = '$data = \App\Models\ContentPostCategory::get();';
        $new->type = 'post_categories';
        $new->save();
        $new = new \App\Models\SettingMenuType();
        $new->name = 'Danh mục sản phẩm';
        $new->code = '$data = \App\Models\ContentProductCategory::get();';
        $new->type = 'product_categories';
        $new->save();
    }
}
