<?php

use Illuminate\Database\Seeder;

class MenuPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new \App\Models\SettingMenuPosition();
        $data->name = 'Menu vị trí đầu';
        $data->code = 'menu_head';
        $data->save();

        $data = new \App\Models\SettingMenuPosition();
        $data->name = 'Menu vị trí cuối trang';
        $data->code = 'menu_footer';
        $data->save();
    }
}
