<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Quản trị viên';
        $user->username = 'admin';
        $user->password = Hash::make('123456a@');
        $user->remember_token = Str::random(100);
        $user->save();
    }
}
