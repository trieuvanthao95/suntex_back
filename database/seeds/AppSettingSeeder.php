<?php

use Illuminate\Database\Seeder;

class AppSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $as = new \App\Models\SettingDefault();
        $as->name = 'favicon';
        $as->value = '/images/favicon.ico';
        $as->save();
        $as = new \App\Models\SettingDefault();
        $as->name = 'phone';
        $as->value = '';
        $as->save();
        $as = new \App\Models\SettingDefault();
        $as->name = 'master_email';
        $as->value = '';
        $as->save();
        $as = new \App\Models\SettingDefault();
        $as->name = 'master_name';
        $as->value = '';
        $as->save();
    }
}
