<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UpdateNameProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $getlist =  \App\Models\ProductType::all();
        foreach ($getlist as $g){
            $p = new \App\Models\ProductType();
            $p->find($g->id);
            $name = trim(str_replace("Mã",'',$g->name));
            DB::table('product_types')
                ->where('id', $g->id)
                ->update(['name' => $name]);
        }

    }
}
