<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id')->default(0);
            $table->string('category_slug')->nullable()->index();
            $table->string('name');
            $table->string('slug');
            $table->string('image')->nullable();
            $table->string('image_zip')->nullable();
            $table->string('alt')->nullable();
            $table->string('image2')->nullable();
            $table->string('image2_zip')->nullable();
            $table->string('alt2')->nullable();
            $table->text('summary')->nullable();
            $table->string('code')->nullable();
            $table->string('packing')->nullable();
            $table->string('weight')->nullable();
            $table->integer('price')->nullable();
            $table->integer('order')->nullable();
            $table->text('attribute_additional')->nullable();
            $table->boolean('published')->default(1)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_products');
    }
}
