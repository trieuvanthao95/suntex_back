<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('slug')->index();
            $table->integer('category_id')->nullable()->index();
            $table->string('category_slug')->nullable()->index();
            $table->boolean('published')->default(1)->index();
            $table->integer('order')->default(0)->index();
            $table->text('summary')->nullable();
            $table->string('image')->nullable();
            $table->string('alt')->nullable();
            $table->string('image_zip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_posts');
    }
}
