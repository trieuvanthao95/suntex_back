<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('content_banner_group_id');
            $table->string('name');
            $table->string('image');
            $table->string('image_zip');
            $table->integer('priority')->default(0);
            $table->string('alt')->default('');
            $table->string('href')->nullable();
            $table->string('summary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_banners');
    }
}
