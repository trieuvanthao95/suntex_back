const gulp = require('gulp');
let concat = require('gulp-concat');
let cleanCSS = require('gulp-clean-css');
const minify = require('gulp-minify');

gulp.task('js', function () {
    return gulp.src([
        'public/js/jquery-3.1.1.js',
        'public/js/popper.min.js',
        'public/js/bootstrap.min.js',
        'public/js/owl.carousel.min.js',
        'public/js/main.js'
    ])
        .pipe(concat('app.js'))
        .pipe(minify())
        .pipe(gulp.dest('public/build'));
});

gulp.task('css', function () {
    return gulp.src([
        'public/css/fontawesome.min.css',
        'public/css/owl.carousel.min.css',
        'public/css/owl.theme.default.css',
        'public/css/jquery.fancybox.min.css',
        'public/css/bootstrap.min.css',
        'public/css/common.css',
        'public/css/style.css',
        'public/css/responsive.css'
    ])
        .pipe(concat('app.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('public/build'));
});
