<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentAttributeOption extends Model
{
    public function attribute()
    {
        return $this->belongsTo(ContentAttribute::class);
    }
}
