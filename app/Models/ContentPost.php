<?php

namespace App\Models;

use App\Utils\StringUtil;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ContentPost extends Model
{
    protected $casts = [
        'published' => 'boolean'
    ];

    protected $appends = [
        'full_path',
        'image_full_path',
        'image_full_path_zip'
    ];

    //Attributes
    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(config('app.url'), $this->attributes['slug'] );
    }

    public function getImageFullPathAttribute()
    {
        if (isset($this->attributes['image'])) {
            if (!Str::startsWith($this->attributes['image'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image']);
            }

            return $this->attributes['image'];
        } else {
            return $this->attributes['image'] = null;
        }
    }

    public function getImageFullPathZipAttribute()
    {
        if (isset($this->attributes['image_zip'])) {
            if (!Str::startsWith($this->attributes['image_zip'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image_zip']);
            }

            return $this->attributes['image_zip'];
        } else {
            return $this->attributes['image_zip'] = null;
        }

    }

    public function getCreatedDateAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->toDateString();
    }

    //Relations
    public function article()
    {
        return $this->morphOne(ContentArticle::class, 'articleable');
    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }

    public function category()
    {
        return $this->belongsTo(ContentPostCategory::class, 'category_id');
    }

    public function gallery()
    {
        return $this->morphOne(ContentGallery::class, 'galleryable');
    }

    public function structured_datas()
    {
        return $this->morphMany(StructuredData::class, 'structureble');
    }

    public function related_posts()
    {
        return $this->belongsToMany(ContentPost::class, 'content_related_posts', 'content_post_id', 'related_post_id');
    }

    public function tags()
    {
        return $this->belongsToMany(ContentPostTag::class, 'content_post_tag', 'content_post_id', 'content_tag_id');
    }

    public function comments()
    {
        return $this->hasManyThrough(ContentComment::class, ContentArticle::class, 'articleable_id')
            ->where('articleable_type', array_search(static::class, Relation::morphMap()) ?: static::class);
    }

    public function public_comments()
    {
        return $this->comments()->published();
    }

    public function labels()
    {
        return $this->morphToMany(SettingLabel::class, 'labelable');
    }

    //Scopes
    public function scopePublished($q)
    {
        return $q->where('published', true);
    }

    public function scopeDraft($q)
    {
        return $q->where('published', false);
    }

    public static function get_post_by_label($label = null, $limit = null)
    {
        if ($label) {
            $query = (new ContentPost())->where('published', 1)
                ->whereHas('labels', function ($q) use ($label) {
                    $q->where('name', $label);
                })->with('labels')->orderBy('order', 'ASC')->orderBy("id",'DESC');
            if ($limit) {
                $postlist = $query->limit($limit)->get();
            } else $postlist = $query->get();
            return $postlist;
        }
    }
    public static function get_list($category_id = null, $label = null, $limit = null)
    {
        $query = (new ContentPost())->where('published', 1)->with('article');
        if ($label) {
            $query->whereHas('labels', function ($q) use ($label) {
                $q->where('name', $label);
            });
        }
        if (!is_null($category_id)) {
            if(is_array($category_id)){
                $query->whereIn('category_id', $category_id);
            }
            else $query->where('category_id', $category_id);
        };
        $query->orderBy("order",'ASC')->orderBy("id",'DESC');
        if ($limit) {
            $catlist = $query->limit($limit)->get()->toArray();
        } else $catlist = $query->get()->toArray();
        return $catlist;
    }
}
