<?php

namespace App\Models;

use App\Utils\StringUtil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ContentPostCategory extends Model
{
    protected $casts = [
        'published' => 'boolean'
    ];

    protected $appends = [
        'full_path',
        'image_full_path',
        'image_full_path_zip'
    ];

    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(config('app.url'), $this->attributes['slug'] );
    }

    public function getImageFullPathAttribute()
    {
        if(isset($this->attributes['image'])){
            if (!Str::startsWith($this->attributes['image'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image']);
            }

            return $this->attributes['image'];
        }else{
            return $this->attributes['image'] = null;
        }
    }

    public function getImageFullPathZipAttribute()
    {
        if (isset($this->attributes['image_zip'])){
            if (!Str::startsWith($this->attributes['image_zip'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image_zip']);
            }

            return $this->attributes['image_zip'];
        }else{
            return $this->attributes['image_zip'] = null;
        }

    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }

    public function posts()
    {
        return $this->hasMany(ContentPost::class, 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo(ContentPostCategory::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(ContentPostCategory::class, 'parent_id');
    }

    public function labels(){
        return $this->morphToMany(SettingLabel::class, 'labelable');
    }

    //Scopes
    public function scopePublished($q)
    {
        return $q->where('published', true);
    }

    public function scopeDraft($q)
    {
        return $q->where('published', false);
    }

    public function get_all_parentid(&$catparentid, $catid = NULL)
    {
        if ($catid) {
            $catid = intval($catid);
            $parents = ContentPostCategory::where('id', $catid)->get();
            if($parents) {
                foreach ($parents as $parent) {
                    if ($parent->parent_id > 0) {
                        $catparentid[] = $parent['parent_id'];
                        $this->get_all_parentid($catparentid, $parent['parent_id']);
                    }
                }
            }
        }
    }

    public static function get_list($parent_id = null, $label = null, $limit = null)
    {
        $query = (new ContentPostCategory())->where('published', 1);
        if ($label) {
            $query->whereHas('labels', function ($q) use ($label) {
                $q->where('name', $label);
            });
        }
        if (!is_null($parent_id)) {
            if(is_array($parent_id)){
                $query->whereIn('parent_id', $parent_id);
            }
            else $query->where('parent_id', $parent_id);
        };
        $query->orderBy("id",'DESC');
        if ($limit) {
            $catlist = $query->limit($limit)->get();
        } else $catlist = $query->get();
        return $catlist;
    }
}
