<?php

namespace App\Models;

use App\Utils\StringUtil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ContentProduct extends Model
{
    protected $casts = [
        'published' => 'boolean',
        'attribute_default' => 'array',
        'attribute_additional' => 'array',
    ];

    protected $appends = [
        'full_path',
        'image_full_path',
        'image2_full_path',
        'image_full_path_zip',
        'image2_full_path_zip',
    ];

    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(
                config('app.url'), $this->attributes['slug']
            );
    }

    public function getImageFullPathAttribute()
    {
        if(isset($this->attributes['image'])){
            if (!Str::startsWith($this->attributes['image'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image']);
            }

            return $this->attributes['image'];
        }else{
            return $this->attributes['image'] = null;
        }
    }

    public function getImage2FullPathAttribute()
    {
        if(isset($this->attributes['image2'])){
            if (!Str::startsWith($this->attributes['image2'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image2']);
            }

            return $this->attributes['image2'];
        }else{
            return $this->attributes['image2'] = null;
        }
    }

    public function getImageFullPathZipAttribute()
    {
        if (isset($this->attributes['image_zip'])){
            if (!Str::startsWith($this->attributes['image_zip'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image_zip']);
            }

            return $this->attributes['image_zip'];
        }else{
            return $this->attributes['image_zip'] = null;
        }
    }

    public function getImage2FullPathZipAttribute()
    {
        if (isset($this->attributes['image2_zip'])){
            if (!Str::startsWith($this->attributes['image2_zip'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image2_zip']);
            }

            return $this->attributes['image2_zip'];
        }else{
            return $this->attributes['image2_zip'] = null;
        }

    }

    public function article()
    {
        return $this->morphOne(ContentArticle::class, 'articleable');
    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }

    public function category()
    {
        return $this->belongsToMany(ContentProductCategory::class, 'category_product', 'product_id', 'category_id');
    }

    public function scopePublished($q)
    {
        return $q->where('published', true);
    }

    public function scopeDraft($q)
    {
        return $q->where('published', false);
    }

    public function comments()
    {
        return $this->hasManyThrough(ContentComment::class, ContentArticle::class, 'articleable_id')
            ->where('articleable_type', array_search(static::class, Relation::morphMap()) ?: static::class);
    }

    public function public_comments()
    {
        return $this->comments()->published();
    }

    public function structured_datas()
    {
        return $this->morphMany(StructuredData::class,'structureble');
    }

    public function labels()
    {
        return $this->morphToMany(SettingLabel::class, 'labelable');
    }

    public function gallery()
    {
        return $this->morphOne(ContentGallery::class, 'galleryable');
    }

    public static function getlist($category_id = null,$label = null, $limit = null)
    {
        $query = (new ContentProduct())->where('published', 1);
        if($label){
            $query->whereHas('labels', function ($q) use ($label) {
                $q->where('name', $label);
            });
        };
        if (!is_null($category_id)) {
            if(is_array($category_id)){
                $query->whereIn('category_id', $category_id);
            }
            else $query->where('category_id', $category_id);
        };
        $query->orderBy('order', 'DESC')->orderBy("name",'ASC');
        if ($limit) {
            $getlist = $query->limit($limit)->get();
        } else $getlist = $query->get();
        return $getlist;
    }

    public static function get_product_by_label($label = null, $limit = null)
    {
        if ($label) {
            $query = (new ContentProduct())->where('published', 1)
                ->whereHas('labels', function ($q) use ($label) {
                    $q->where('name', $label);
                })->with('labels')->orderBy("name",'ASC');
            if ($limit) {
                $list = $query->limit($limit)->get();
            } else $list = $query->get();
            return $list;
        }
    }
}
