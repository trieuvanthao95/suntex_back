<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingLanguage extends Model
{
    static  function get_code(){
        $elements = SettingLanguage::all();
        $config = [];
        foreach ($elements as $d) {
            $config[$d['name']] = $d['translate'];
        }
        return $config;
    }
}
