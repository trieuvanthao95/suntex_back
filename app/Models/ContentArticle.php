<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentArticle extends Model
{
    use SoftDeletes;

    public function articleable()
    {
        return $this->morphTo();
    }

    public function comments()
    {
        return $this->hasMany(ContentComment::class, 'content_article_id');
    }

    public function root_comments()
    {
        return $this->hasMany(ContentComment::class, 'content_article_id')->isRoot();
    }
}
