<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingLabel extends Model
{
    public function posts()
    {
        return $this->morphedByMany(ContentPost::class, 'labelable');
    }

    public function services()
    {
        return $this->morphedByMany(ContentService::class, 'labelable');
    }

    public function products()
    {
        return $this->morphedByMany(ContentProduct::class, 'labelable');
    }
}
