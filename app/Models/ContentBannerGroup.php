<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentBannerGroup extends Model
{
    public function banners()
    {
        return $this->hasMany(ContentBanner::class, 'content_banner_group_id');
    }
    static function get_code($code=null){
        if($code){
            $query = ContentBannerGroup::where("code",$code)->with(['banners' => function ($q) {
                $q->orderBy('priority', 'desc');
            }])
                ->first();
           if($query) return $query['banners'];
        }
    }
}
