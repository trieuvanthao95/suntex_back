<?php

namespace App\Models;

use App\Utils\StringUtil;
use Illuminate\Database\Eloquent\Model;

class ContentTag extends Model
{
    protected $appends = [
        'full_path'
    ];

    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(
            config('app.url'),
            'tag', $this->attributes['slug']
        );
    }

    public function posts()
    {
        return $this->morphedByMany(ContentPost::class, 'tagable');
    }

    public function services()
    {
        return $this->morphedByMany(ContentService::class, 'tagable');
    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }
}
