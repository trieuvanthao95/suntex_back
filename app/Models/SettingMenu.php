<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingMenu extends Model
{
    public function position()
    {
        return $this->belongsTo(SettingMenuPosition::class, 'menu_position_id');
    }

    public function menuable()
    {
        return $this->morphTo();
    }

    public function parent()
    {
        return $this->belongsTo(SettingMenu::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(SettingMenu::class, 'parent_id');
    }
}
