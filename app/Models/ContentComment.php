<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentComment extends Model
{
    protected $casts = [
        'published' => 'boolean'
    ];

    public function article()
    {
        return $this->belongsTo(ContentArticle::class, 'content_article_id');
    }

    public function parent()
    {
        return $this->belongsTo(ContentComment::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(ContentComment::class, 'parent_id');
    }

    public function scopeIsRoot($q)
    {
        return $q->where('parent_id', 0);
    }

    public function scopePublished($q)
    {
        return $q->where('published', true);
    }

    public function scopeDraft($q)
    {
        return $q->where('published', false);
    }
}
