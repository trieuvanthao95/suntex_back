<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingWidget extends Model
{
    static function get_code($code=null){
        if($code){
            $query = (new SettingWidget())->where('name', $code)->first();
            return $query;
        }
    }
}
