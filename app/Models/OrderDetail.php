<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public function order(){
        return $this->hasMany(OrderDetail::class);
    }

    public function product(){
        return $this->belongsTo(ContentProduct::class);
    }

    public function type(){
        return $this->belongsTo(ProductType::class, 'type');
    }
}
