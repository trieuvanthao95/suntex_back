<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ContentPhoto extends Model
{
    protected $appends = [
        'image_full_path',
        'image_full_path_zip'
    ];

    public function photoable()
    {
        return $this->morphTo();
    }

    public function getImageFullPathAttribute()
    {
        if (!Str::startsWith($this->attributes['image'], 'http')) {
            return Storage::disk('public')->url($this->attributes['image']);
        }

        return $this->attributes['image'];
    }

    public function getImageFullPathZipAttribute()
    {
        if($this->attributes['image_zip']){
            if (!Str::startsWith($this->attributes['image_zip'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image_zip']);
            }

            return $this->attributes['image_zip'];
        }
    }
}
