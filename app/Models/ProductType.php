<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductType extends Model
{
    protected $casts = [
        'published' => 'boolean'
    ];

    protected $appends = [
        'image_full_path',
        'image_full_path_zip'
    ];

    public function getImageFullPathAttribute()
    {
        if (isset($this->attributes['image'])) {
            if (!Str::startsWith($this->attributes['image'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image']);
            }

            return $this->attributes['image'];
        } else {
            return $this->attributes['image'] = null;
        }
    }

    public function getImageFullPathZipAttribute()
    {
        if (isset($this->attributes['image_zip'])) {
            if (!Str::startsWith($this->attributes['image_zip'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image_zip']);
            }

            return $this->attributes['image_zip'];
        } else {
            return $this->attributes['image_zip'] = null;
        }

    }
}
