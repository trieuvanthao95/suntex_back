<?php

namespace App\Models;

use App\Utils\StringUtil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ContentService extends Model
{
    protected $casts = [
        'published' => 'boolean'
    ];

    protected $appends = [
        'full_path',
        'image_full_path',
        'image_full_path_zip'
    ];

    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(
            config('app.url'), $this->attributes['category_slug'], $this->attributes['slug'] . '.html'
        );
    }

    public function getImageFullPathAttribute()
    {
        if (isset($this->attributes['image'])) {
            if (!Str::startsWith($this->attributes['image'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image']);
            }

            return $this->attributes['image'];
        } else {
            return $this->attributes['image'] = null;
        }
    }

    public function getImageFullPathZipAttribute()
    {
        if (isset($this->attributes['image_zip'])) {
            if (!Str::startsWith($this->attributes['image_zip'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image_zip']);
            }

            return $this->attributes['image_zip'];
        } else {
            return $this->attributes['image_zip'] = null;
        }

    }

    public function article()
    {
        return $this->morphOne(ContentArticle::class, 'articleable');
    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }

    public function category()
    {
        return $this->belongsTo(ContentServiceCategory::class, 'category_id');
    }

    public function structured_datas()
    {
        return $this->morphMany(StructuredData::class,'structureble');
    }

    public function comments()
    {
        return $this->hasManyThrough(ContentComment::class, ContentArticle::class, 'articleable_id')
            ->where('articleable_type', array_search(static::class, Relation::morphMap()) ?: static::class);
    }

    public function public_comments()
    {
        return $this->comments()->published();
    }

    public function scopePublished($q)
    {
        return $q->where('published', true);
    }

    public function scopeDraft($q)
    {
        return $q->where('published', false);
    }

    public function labels()
    {
        return $this->morphToMany(SettingLabel::class, 'labelable');
    }
}
