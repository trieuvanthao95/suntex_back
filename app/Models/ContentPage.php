<?php

namespace App\Models;

use App\Utils\StringUtil;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ContentPage extends Model
{
    protected $casts = [
        'published' => 'boolean'
    ];

    protected $appends = [
        'full_path'
    ];

    //Attributes
    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(config('app.url'), $this->attributes['slug']);
    }

    public function getCreatedDateAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->toDateString();
    }

    public function getSummaryAttribute()
    {
        $summary = $this->meta->description ?? strip_tags(html_entity_decode($this->attributes['content']));
        return mb_substr($summary, 0, 200) . '...';
    }

    //Relations
    public function article()
    {
        return $this->morphOne(ContentArticle::class, 'articleable');
    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }

    public function structured_datas()
    {
        return $this->morphMany(StructuredData::class,'structureble');
    }

    //Scopes
    public function scopePublished($q)
    {
        return $q->where('published', true);
    }

    public function scopeDraft($q)
    {
        return $q->where('published', false);
    }
}
