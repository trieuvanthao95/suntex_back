<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingMenuPosition extends Model
{
    public function menus()
    {
        return $this->hasMany(SettingMenu::class, 'menu_position_id');
    }

    static function get_menu(){
        $elements = SettingMenuPosition::with(['menus' => function ($q) {
            $q->where('parent_id', 0)->with(['children' => function ($q) {
                $q->with(['children' => function ($q) {
                    $q->orderBy('order', 'asc');
                }])->orderBy('order', 'asc');
            }])->orderBy('order', 'asc');
        }])->get();
        $config = [];
        foreach ($elements as $d) {
            if (isset($d->menus)) {
                $data = self::generateMenu($d->menus);
                $config[$d['code']] = $data;
            }
        }
        return $config;
    }

    public static function generateMenu($contents)
    {
        $output = [];
        foreach ($contents as $content) {
            if ($content->menuable_type == null) {
                if (count($content->children) > 0) {
                    $child = self::generateMenu($content->children);
                    array_push($output, [
                        'menuable_type' => $content->menuable_type,
                        'menuable_id' => $content->url,
                        'name' => $content->name,
                        'url' => $content->url,
                        'children' => $child
                    ]);
                } else {
                    array_push($output, [
                        'menuable_type' => $content->menuable_type,
                        'menuable_id' => $content->url,
                        'name' => $content->name,
                        'url' => $content->url
                    ]);
                }
            } elseif ($content->menuable_type != null) {
                $menu_type = SettingMenuType::where('type', $content->menuable_type)->first();
                $data = null;
                eval($menu_type->code);
                foreach ($data as $d) {
                    if ($d->id == $content->menuable_id) {
                        $data = $d;
                        break;
                    }
                }
                if (count($content->children) > 0) {
                    $child = self::generateMenu($content->children);
                    if (isset($data)) {
                        array_push($output, [
                            'menuable_type' => $content->menuable_type,
                            'menuable_id' => $content->menuable_id,
                            'name' => $content->name ?? $data->name,
                            'url' => $content->url ?? $data->full_path,
                            'children' => $child
                        ]);
                    }
                } else {
                    if (isset($data)) {
                        array_push($output, [
                            'menuable_type' => $content->menuable_type,
                            'menuable_id' => $content->menuable_id,
                            'name' => $content->name ?? $data->name,
                            'url' => $content->url ?? $data->full_path
                        ]);
                    }
                }
            }
        }
        return $output;
    }
}
