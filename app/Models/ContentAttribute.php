<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentAttribute extends Model
{
    public function options()
    {
        return $this->hasMany(ContentAttributeOption::class);
    }
}
