<?php

namespace App\Models;

use App\Utils\StringUtil;
use Illuminate\Database\Eloquent\Model;

class ContentPostTag extends Model
{
    protected $appends = [
        'full_path'
    ];

    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(
            config('app.url'),
            'tag', $this->attributes['slug']
        );
    }

    public function posts()
    {
        return $this->belongsToMany(ContentPost::class, 'content_post_tag', 'content_tag_id', 'content_post_id');
    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }
}
