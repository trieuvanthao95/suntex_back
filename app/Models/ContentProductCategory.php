<?php

namespace App\Models;

use App\Utils\StringUtil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ContentProductCategory extends Model
{
    protected $casts = [
        'published' => 'boolean'
    ];

    protected $appends = [
        'full_path',
        'image_full_path',
        'image_full_path_zip'
    ];

    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(config('app.url'), $this->attributes['slug']);
    }

    public function getImageFullPathAttribute()
    {
        if (isset($this->attributes['image'])) {
            if (!Str::startsWith($this->attributes['image'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image']);
            }

            return $this->attributes['image'];
        } else {
            return $this->attributes['image'] = null;
        }
    }

    public function getImageFullPathZipAttribute()
    {
        if (isset($this->attributes['image'])) {
            if (!Str::startsWith($this->attributes['image'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image']);
            }

            return $this->attributes['zip_url'];
        } else {
            return $this->attributes['zip_url'] = null;
        }
    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }

    public function products()
    {
        return $this->belongsToMany(ContentProduct::class, 'category_product', 'category_id', 'product_id');
    }

    public function parent()
    {
        return $this->belongsTo(ContentProductCategory::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(ContentProductCategory::class, 'parent_id');
    }

    public function labels()
    {
        return $this->morphToMany(SettingLabel::class, 'labelable');
    }

    //Scopes
    public function scopePublished($q)
    {
        return $q->where('published', true);
    }

    public function scopeDraft($q)
    {
        return $q->where('published', false);
    }

    public function get_all_parentid(&$catparentid, $catid = NULL)
    {
        if ($catid) {
            $catid = intval($catid);
            $parents = ContentProductCategory::where('id', $catid)->get();
            if ($parents) {
                foreach ($parents as $parent) {
                    if ($parent->parent_id > 0) {
                        $catparentid[] = $parent['parent_id'];
                        $this->get_all_parentid($catparentid, $parent['parent_id']);
                    }
                }
            }
        }
    }

    public function get_all_childs(&$catchildid,$catid=NULL){
        if($catid) {
            $childs = ContentProductCategory::where('parent_id', $catid)->where('published',1)->get();
            if($childs){
                foreach ($childs as $child) {
                    $catchildid[] = $child['id'];
                    $this->get_all_childs($catchildid, $child['id']);
                }
            }
        }
    }


    public static function get_category_by_label($label = null, $limit = null)
    {
        if ($label) {
            $query = (new ContentProductCategory())->where('published', 1)
                ->whereHas('labels', function ($q) use ($label) {
                    $q->where('name', $label);
                })->with('labels')->orderBy('order','ASC')->orderBy("id",'DESC');
            if ($limit) {
                $list = $query->limit($limit)->get();
            } else $list = $query->get();
            return $list;
        }
    }

    public static function get_list($parent_id = null, $label = null, $limit = null)
    {
        $query = (new ContentProductCategory())->where('published', 1);
        if ($label) {
            $query->whereHas('labels', function ($q) use ($label) {
                $q->where('name', $label);
            });
        }
        if (!is_null($parent_id)) {
            if(is_array($parent_id)){
                $query->whereIn('parent_id', $parent_id);
            }
            else $query->where('parent_id', $parent_id);
        };
        $query->orderBy("id",'DESC');
        if ($limit) {
            $catlist = $query->limit($limit)->get();
        } else $catlist = $query->get();
        return $catlist;
    }
}
