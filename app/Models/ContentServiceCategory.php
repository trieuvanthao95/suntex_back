<?php

namespace App\Models;

use App\Utils\StringUtil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ContentServiceCategory extends Model
{
    protected $casts = [
        'published' => 'boolean'
    ];

    protected $appends = [
        'full_path',
        'image_full_path',
        'image_full_path_zip'
    ];

    public function getFullPathAttribute()
    {
        return StringUtil::joinPaths(config('app.url'), $this->attributes['slug'].'/');
    }

    public function getImageFullPathAttribute()
    {
        if(isset($this->attributes['image'])){
            if (!Str::startsWith($this->attributes['image'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image']);
            }

            return $this->attributes['image'];
        }else{
            return $this->attributes['image'] = null;
        }
    }

    public function getImageFullPathZipAttribute()
    {
        if (isset($this->attributes['image_zip'])){
            if (!Str::startsWith($this->attributes['image_zip'], 'http')) {
                return Storage::disk('public')->url($this->attributes['image_zip']);
            }

            return $this->attributes['image_zip'];
        }else{
            return $this->attributes['image_zip'] = null;
        }

    }

    public function meta()
    {
        return $this->morphOne(SeoMeta::class, 'metaable');
    }

    public function services()
    {
        return $this->hasMany(ContentService::class, 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo(ContentServiceCategory::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(ContentServiceCategory::class, 'parent_id');
    }

    public function labels(){
        return $this->morphToMany(SettingLabel::class, 'labelable');
    }

    //Scopes
    public function scopePublished($q)
    {
        return $q->where('published', true);
    }

    public function scopeDraft($q)
    {
        return $q->where('published', false);
    }

    public function get_all_parentid(&$catparentid, $catid = NULL)
    {
        if ($catid) {
            $catid = intval($catid);
            $parents = ContentPostCategory::where('id', $catid)->get();
            if($parents) {
                foreach ($parents as $parent) {
                    if ($parent->parent_id > 0) {
                        $catparentid[] = $parent['parent_id'];
                        $this->get_all_parentid($catparentid, $parent['parent_id']);
                    }
                }
            }
        }
    }

    public  static  function get_list($limit=null){
        $query = (new ContentServiceCategory())->where('published', 1);
        if ($limit) {
            $news_categories = $query->limit($limit)->get();
        } else $news_categories = $query->get();
        return $news_categories;
    }
}
