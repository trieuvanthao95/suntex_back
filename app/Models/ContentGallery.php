<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentGallery extends Model
{
    protected $casts = [
        'published' => 'boolean',
    ];

    public function images()
    {
        return $this->morphMany(ContentPhoto::class, 'photoable');
    }

    public function galleryable()
    {
        return $this->morphTo();
    }

}
