<?php

namespace App\Providers;

use App\Models\SettingDefault;
use App\Models\SettingLanguage;
use App\Models\SettingMenuPosition;
use App\Models\SettingWidget;
use App\Utils\CacheUtil;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::share('settings',CacheUtil::getAppSetting());
        View::share('language',CacheUtil::getLanguage());
        View::share('menus',CacheUtil::getMenu());
        View::share('widgets',CacheUtil::getWidget());
        View::composer('*', 'App\Http\ViewComposers\AllViewComposer');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
