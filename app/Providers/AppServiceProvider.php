<?php

namespace App\Providers;

use App\Models\ContentArticle;
use App\Models\ContentBanner;
use App\Models\ContentCompanyBranch;
use App\Models\ContentGallery;
use App\Models\ContentPage;
use App\Models\ContentPartner;
use App\Models\ContentPhoto;
use App\Models\ContentPost;
use App\Models\ContentPostCategory;
use App\Models\ContentPostTag;
use App\Models\ContentProduct;
use App\Models\ContentProductCategory;
use App\Models\ContentService;
use App\Models\ContentServiceCategory;
use App\Models\ContentStaff;
use App\Models\ContentTag;
use App\Observers\ArticleObserver;
use App\Observers\PhotoObserver;
use App\Observers\PostCategoryObserver;
use App\Observers\PostObserver;
use App\Observers\PostTagObserver;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ContentPost::observe(PostObserver::class);
        ContentPostCategory::observe(PostCategoryObserver::class);
        ContentPostTag::observe(PostTagObserver::class);
        ContentArticle::observe(ArticleObserver::class);
        ContentPhoto::observe(PhotoObserver::class);

        Relation::morphMap([
            'articles' => ContentArticle::class,
            'galleries' => ContentGallery::class,
            'posts' => ContentPost::class,
            'post_categories' => ContentPostCategory::class,
            'post_tags' => ContentPostTag::class,
            'tags' => ContentTag::class,
            'banners' => ContentBanner::class,
            'services' => ContentService::class,
            'service_categories' => ContentServiceCategory::class,
            'products' => ContentProduct::class,
            'product_categories' => ContentProductCategory::class,
            'pages' => ContentPage::class,
            'staffs' => ContentStaff::class,
            'partners' => ContentPartner::class,
            'company_branches' => ContentCompanyBranch::class
        ]);
    }
}
