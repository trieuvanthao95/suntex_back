<?php
/**
 * Created by PhpStorm.
 * User: BaoHoang
 * Date: 6/16/2019
 * Time: 4:31 PM
 */

namespace App\Observers;


use App\Models\ContentPost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostObserver
{
    public function deleting(ContentPost $model)
    {
        try {
            DB::beginTransaction();
            if (isset($model->photo)) {
                $model->photo->delete();
            }
            $model->tags()->detach();
            $model->meta()->delete();
            $model->article()->delete();
            DB::commit();
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            throw $e;
        }

    }
}
