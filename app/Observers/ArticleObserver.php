<?php

namespace App\Observers;

use App\Models\ContentArticle;

class ArticleObserver
{
    public function deleting(ContentArticle $article)
    {
        $article->comments()->delete();
    }
}
