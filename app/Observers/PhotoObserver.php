<?php

namespace App\Observers;

use App\Models\ContentPhoto;
use App\Utils\FileUtil;

class PhotoObserver
{

    public function deleting(ContentPhoto $photo)
    {
        FileUtil::removePublicFile($photo->url);
    }

}
