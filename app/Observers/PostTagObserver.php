<?php
/**
 * Created by PhpStorm.
 * User: BaoHoang
 * Date: 6/16/2019
 * Time: 4:31 PM
 */

namespace App\Observers;


use App\Models\ContentPostTag;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostTagObserver
{

    public function deleting(ContentPostTag $model)
    {
        try {
            DB::beginTransaction();
            $model->posts()->detach();
            $model->meta()->delete();
            DB::commit();
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            throw $e;
        }

    }

}
