<?php
/**
 * Created by PhpStorm.
 * User: BaoHoang
 * Date: 6/16/2019
 * Time: 4:31 PM
 */

namespace App\Observers;


use App\Models\ContentPostCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostCategoryObserver
{
    public function updated(ContentPostCategory $model)
    {
        $model->posts()->update(['published' => $model->published]);
    }

    public function deleting(ContentPostCategory $model)
    {
        try {
            DB::beginTransaction();
            if (isset($model->photo)) {
                $model->photo->delete();
            }
            $model->children()->update(['parent_id' => 0, 'parent_path' => null]);
            $model->posts()->update(['category_id' => 0, 'category_slug' => '', 'published' => false]);
            $model->meta()->delete();
            DB::commit();
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            throw $e;
        }

    }

}
