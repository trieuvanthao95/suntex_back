<?php

namespace App\Observers;

use App\Models\CategoryRate;
use App\Models\ProductCategoryRate;

class CategoryRateObserver
{
    /**
     * Handle the category rate "created" event.
     *
     * @param  \App\CategoryRate  $categoryRate
     * @return void
     */
    public function created(CategoryRate $categoryRate)
    {
        $rate_average = ProductCategoryRate::where('category_id', $categoryRate->category_id)->first();
        if ($rate_average) {
            $rate = 0;
            $comments = \App\Models\CategoryRate::where('category_id', $categoryRate->category_id)->get();
            if (count($comments) > 0) {
                $total_rate = 0;
                for ($i = 0; $i < count($comments); $i++) {
                    $total_rate = $total_rate + $comments[$i]['rate'];
                }
                $rate = round((($total_rate / count($comments)) / 5), 1) * 5;
            }
            $rate_average->rate = $rate;
            $rate_average->total = count($comments);
            $rate_average->save();
        } else {
            $rate = 0;
            $comments = CategoryRate::where('category_id', $categoryRate->category_id)->get();
            if (count($comments) > 0) {
                $total_rate = 0;
                for ($i = 0; $i < count($comments); $i++) {
                    $total_rate = $total_rate + $comments[$i]['rate'];
                }
                $rate = round((($total_rate / count($comments)) / 5), 1) * 5;
            }
            $rate_average = new ProductCategoryRate();
            $rate_average->category_id = $categoryRate->category_id;
            $rate_average->rate = $rate;
            $rate_average->total = count($comments);
            $rate_average->save();
        }
    }

    /**
     * Handle the category rate "updated" event.
     *
     * @param  \App\CategoryRate  $categoryRate
     * @return void
     */
    public function updated(CategoryRate $categoryRate)
    {
        $rate_average = ProductCategoryRate::where('category_id', $categoryRate->category_id)->first();
        if ($rate_average) {
            $rate = 0;
            $comments = CategoryRate::where('category_id', $categoryRate->category_id)->get();
            if (count($comments) > 0) {
                $total_rate = 0;
                for ($i = 0; $i < count($comments); $i++) {
                    $total_rate = $total_rate + $comments[$i]['rate'];
                }
                $rate = round((($total_rate / count($comments)) / 5), 1) * 5;
            }
            $rate_average->rate = $rate;
            $rate_average->total = count($comments);
            $rate_average->save();
        } else {
            $rate = 0;
            $comments = CategoryRate::where('category_id', $categoryRate->category_id)->get();
            if (count($comments) > 0) {
                $total_rate = 0;
                for ($i = 0; $i < count($comments); $i++) {
                    $total_rate = $total_rate + $comments[$i]['rate'];
                }
                $rate = round((($total_rate / count($comments)) / 5), 1) * 5;
            }
            $rate_average = new ProductCategoryRate();
            $rate_average->category_id = $categoryRate->category_id;
            $rate_average->rate = $rate;
            $rate_average->total = count($comments);
            $rate_average->save();
        }
    }

    /**
     * Handle the category rate "deleted" event.
     *
     * @param  \App\CategoryRate  $categoryRate
     * @return void
     */
    public function deleted(CategoryRate $categoryRate)
    {
        $rate_average = ProductCategoryRate::where('category_id', $categoryRate->category_id)->first();
        if ($rate_average) {
            $rate = 0;
            $comments = CategoryRate::where('category_id', $categoryRate->category_id)->get();
            if (count($comments) > 0) {
                $total_rate = 0;
                for ($i = 0; $i < count($comments); $i++) {
                    $total_rate = $total_rate + $comments[$i]['rate'];
                }
                $rate = round((($total_rate / count($comments)) / 5), 1) * 5;
            }
            $rate_average->rate = $rate;
            $rate_average->total = count($comments);
            $rate_average->save();
        } else {
            $rate = 0;
            $comments = CategoryRate::where('category_id', $categoryRate->category_id)->get();
            if (count($comments) > 0) {
                $total_rate = 0;
                for ($i = 0; $i < count($comments); $i++) {
                    $total_rate = $total_rate + $comments[$i]['rate'];
                }
                $rate = round((($total_rate / count($comments)) / 5), 1) * 5;
            }
            $rate_average = new ProductCategoryRate();
            $rate_average->category_id = $categoryRate->category_id;
            $rate_average->rate = $rate;
            $rate_average->total = count($comments);
            $rate_average->save();
        }
    }

    /**
     * Handle the category rate "restored" event.
     *
     * @param  \App\CategoryRate  $categoryRate
     * @return void
     */
    public function restored(CategoryRate $categoryRate)
    {
        //
    }

    /**
     * Handle the category rate "force deleted" event.
     *
     * @param  \App\CategoryRate  $categoryRate
     * @return void
     */
    public function forceDeleted(CategoryRate $categoryRate)
    {
        //
    }
}
