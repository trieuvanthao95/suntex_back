<?php

namespace App\Console\Commands;

use App\Models\Guide;
use App\Models\GuideCategory;
use App\Models\Post;
use App\Models\Tour;
use App\Models\TourCategory;
use App\Utils\UrlUtil;
use App\Utils\StringUtil;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class DataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wayto:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Data description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $guides = Post::get();

        foreach ($guides as $guide) {
            $model = (new Post())->find($guide['id']);
            $article = $model->article;
            $model->slug = Str::slug($article['title']);
            DB::beginTransaction();
            $model->save();
            DB::commit();
        }
        return;
        $guides = Guide::get();

        foreach ($guides as $guide) {
            $model = (new Guide())->find($guide['id']);
            $model->slug = Str::slug($guide['title']);
            DB::beginTransaction();
            $model->save();
            DB::commit();
        }
        return;
        $guides = GuideCategory::get();

        foreach ($guides as $guide) {
            $model = (new GuideCategory())->find($guide['id']);
            $model->slug = Str::slug($guide['name']);
            DB::beginTransaction();
            $model->save();
            DB::commit();
        }


        return;
        $tours = TourCategory::get();

        foreach ($tours as $tour) {
            $model = (new TourCategory())->find($tour['id']);
            $model->category_slug = Str::slug($tour['name']);
            DB::beginTransaction();
            $model->save();
            DB::commit();
        }
        return;
        $tours = Tour::get();

        foreach ($tours as $tour) {
            $model = (new Tour())->find($tour['id']);
            $model->slug = Str::slug($tour['name']);
            DB::beginTransaction();
            $model->save();
            DB::commit();
        }
    }
}
