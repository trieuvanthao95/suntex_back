<?php

namespace App\Http\ViewComposers;

use App\Utils\CacheUtil;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;

class AllViewComposer
{

    public function compose(View $view)
    {
        $settings = [];
        $header_embed_codes = [];
        $footer_embed_codes = [];
        $php_codes = [];
        $allTags = [];
     //   $menus = [];
        $banners = [];
        $widgets = [];
     //   $language = [];
        try {
         //   $settings = CacheUtil::getAppSetting();
            $embed_codes = CacheUtil::getEmbeddedCode();
           // $banners = CacheUtil::getBanner();
          //  $menus = CacheUtil::getMenu();
          //  $widgets = CacheUtil::getWidget();
         //   $language = CacheUtil::getLanguage();
            foreach ($embed_codes as $embed_code) {
                if ($embed_code['index'] == 1) {
                    array_push($header_embed_codes, $embed_code['code']);
                }
                if ($embed_code['index'] == 2) {
                    array_push($footer_embed_codes, $embed_code['code']);
                }
                if ($embed_code['index'] == 3) {
                    array_push($php_codes, $embed_code['code']);
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
        $view->with(compact(
                'header_embed_codes',
                'footer_embed_codes',
                'php_codes'
            )
        );
    }

}
