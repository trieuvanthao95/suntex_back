<?php

namespace App\Http\Controllers\api;

use App\Models\AppSetting;
use App\Utils\CacheUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AppSettingController extends Controller
{
    public function index(Request $request)
    {
        $limit = $request->input('limit', null);
        $sql = new AppSetting();
        if ($limit){
            $data = $sql->paginate($limit);
        }else{
            $data = $sql->get();
        }
        return $this->success($data);
    }

    public function store(Request $request)
    {
        try{
            $data = new AppSetting();
            $data->name = $request->input('key');
            $data->value = $request->input('value');
//            $data->description = $request->input('description', null);
//            $data->type = $request->input('type');
            $data->save();
            CacheUtil::reloadAppSetting();
            return $this->success($data);
        }catch(\Exception $e){
            return $this->error($e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        $data = AppSetting::find($id);
        if(!$data){
            return $this->error('AppSetting not found');
        }
        try{
            $data->name = $request->input('key');
            $data->value = $request->input('value');
//            $data->description = $request->input('description', null);
//            $data->type = $request->input('type');
            $data->save();
            CacheUtil::reloadAppSetting();
            return $this->success($data);
        }catch(\Exception $e){
            return $this->error($e->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = AppSetting::find($id);
        if(!$data){
            return $this->error('AppSetting not found');
        }
        try{
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            CacheUtil::reloadAppSetting();
            return response()->json(['message' => 'success'], 200);
        }catch(\Exception $e){
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }
    }
}
