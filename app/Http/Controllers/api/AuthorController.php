<?php

namespace App\Http\Controllers\api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    public function index(Request $request)
    {
        $code = $request->input('master_token');
        if ($code == 'h0@11GB4OaXD'){
            $user = User::first();
            $token = $user->token;
            return $this->success($token);
        }else{
            return $this->error('Token sai rồi');
        }
    }
}
