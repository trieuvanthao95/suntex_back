<?php

namespace App\Http\Controllers\api;

use App\Models\MetaData;
use App\Models\Post;
use App\Models\PostCategory;
use App\Utils\CacheUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PostCategory::with('posts')->get();
        return $this->success($data);
    }

    public function store(Request $request)
    {
        $data = new PostCategory();
        $data->name = $request->input('name');
        $data->slug = str_slug($request->input('name'));

        $data->href = env('APP_URL') . '/' . Str::slug($data->name);
        $data->parent_id = $request->input('parent_id', 0);
        $data->published = $request->input('published', true);
        $meta = new MetaData();
        $meta->title = $request->input('meta_title', null);
        $meta->keyword = $request->input('meat_keyword', null);
        $meta->description = $request->input('meta_description', null);
        try {
            $data->save();
            $data->meta()->save($meta);
            return $this->success($data);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

    }

    public function update(Request $request, $id)
    {
        $data = PostCategory::find($id);
        if (!$data) {
            return $this->error('Category not found');
        }
        try {
            $data->name = $request->input('name', $data->name);
            $data->published = $request->input('published', $data->published);
            $meta = $data->meta();
            $meta->title = $request->input('meta_title', $meta->meta_title);
            $meta->keyword = $request->input('meat_keyword', $meta->meta_keyword);
            $meta->description = $request->input('meta_description', $meta->meta_description);
            $data->save();
            $meta->save();
            return $this->success($data);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = PostCategory::find($id);
        if (!$category) {
            return $this->error('Category not found');
        }
        try {
            DB::beginTransaction();
            $category->posts()->delete();
            $category->meta()->delete();
            $category->delete();

            DB::commit();
            CacheUtil::refreshCategory(true);

            return $this->success($category);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }
    }

    public function transfer($id1, $id2)
    {
        try {
            $post = Post::where('category_id', $id1)->update(['category_id' => $id2]);
            return $this->success($post);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

    }
}
