<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\admin\ArticleCommentController;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleComment;
use App\Models\Comment;
use App\Models\MetaData;
use App\Models\Photo;
use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $limit = $request->input('limit', null);
        $status = $request->input('status', null);
        $category_id = $request->input('category_id', null);
        $start_date = $request->input('start_date', null);
        $end_date = $request->input('end_date', null);
        $sql = Post::when($category_id, function ($q) use ($category_id) {
            return $q->where('category_id', $category_id);
        })->when($status, function ($q) use ($status) {
            return $q->where('published', $status);
        })->with(['article', 'meta', 'photo', 'category', 'tags']);
        if ($limit) {
            $data = $sql->paginate($limit);
        } else {
            $data = $sql->get();
        }
        return $this->success($data);
    }

    public function store(Request $request)
    {
        $title = $request->input('title');
        if (empty($title)) {
            return $this->error('Yêu cầu tiêu đề');
        }
        $content = $request->input('content');
        if (empty($content)) {
            return $this->error('Yêu cầu nội dung');
        }
        $authorName = $request->input('author_name', null);
        $authorUrl = $request->input('author_url', null);
        $category_id = $request->input('category_id', 0);

//        $category = PostCategory::find($category_id);

        $meta_title = $request->input('meta_title');
        $meta_keyword = $request->input('meta_keyword', null);
        $meta_description = $request->input('meta_description', null);
        $status = $request->input('status', 0);
        $image = $request->input('image', null);
        $comments = json_decode($request->input('comments', '[]'));

        $post_slug = str_slug($title);
        $post = Post::where('post_slug', $post_slug)->first();
        if ($post) {
            return $this->error('Bài viết đã tồn tại', $post->toArray());
        }
        try {
            DB::beginTransaction();
            $post = new Post();
            $post->slug = $post_slug;
            $post->summary = $request->input('summary', '');
            $post->category_id = $category_id;
            $post->published = $status;
            if ($category_id) {
                $category = PostCategory::find($category_id);
                if ($category) {
                    $post->category_slug = $category->slug;
                    $post->href = env('APP_URL') . '/' . $category->slug . '/' . $post_slug;
                } else {
                    $post->category_id = 0;
                }
            }

            $article = new Article();
            $article->title = $title;
            $article->content = $content;
            $article->author_name = $authorName;
            $article->author_url = $authorUrl;

            $photo = new Photo();
            $photo->alt = $request->input('alt', '');
            $photo->url = $image;
            $photo->zip_url = $image;

            $meta = new MetaData();
            $meta->title = $meta_title;
            $meta->keyword = $meta_keyword;
            $meta->description = $meta_description;

            $post->save();
            $post->photo()->save($photo);
            $post->article()->save($article);
            $post->meta()->save($meta);
            $createComments = [];
            if ($comments && count($comments) > 0) {
                foreach ($comments as $item) {
                    $c = new ArticleComment;
                    $c->author = $item->author_name;
                    $c->content = $item->content;
                    $c->parent_id = 0;
                    $c->published = $item->status ?? 1;
                    array_push($createComments, $c);
                }
                $article = $post->article;
                $article->comments()->saveMany($createComments);
            }
            DB::commit();
            return $this->success($post);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }
    }

    public function show($id)
    {
        $data = Post::where('id', $id)->with(['article', 'meta', 'photo', 'category', 'tags'])->first();
        return $this->success($data);
    }

    public function update(Request $request, $id)
    {
        $title = $request->input('title', null);
        $content = $request->input('content', null);
        $meta_title = $request->input('meta_title', null);
        $meta_keyword = $request->input('meta_keyword', null);
        $meta_description = $request->input('meta_description', null);
        $status = $request->input('status', null);
        $post = Post::find($id);
        if (!$post) {
            return $this->error('Post not found');
        }
        try {
            $article = $post->article;
            $article->title = $title ?? $article->title;
            $article->content = $content ?? $article->content;
            $meta = $post->meta;
            $meta->title = $meta_title ?? $meta->title;
            $meta->keyword = $meta_keyword ?? $meta->keyword;
            $meta->description = $meta_description ?? $meta->description;
            $post->published = $status ?? $post->published;
            $post->save();
            $article->save();
            $meta->save();
            return $this->success($post);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        if (empty($post)) {
            return $this->success('Post not found');
        }
        try {
            $post->article()->delete();
            $post->meta()->delete();
            $post->delete();
            return $this->success($post);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function disable($id)
    {
        $data = Post::find($id);
        if (!$data) {
            return $this->error('Post not found');
        }
        try {
            $data->published = 0;
            $data->save();
            return $this->success($data);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function enable($id)
    {
        $data = Post::find($id);
        if (!$data) {
            return $this->error('Post not found');
        }
        try {
            $data->published = 1;
            $data->save();
            $this->success($data);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function bulk(Request $request)
    {
        $data = json_decode($request->input('data'), true);
        $results = [];
        foreach ($data as $i => $d) {
            $method = $d['method'];
            if ($method == 'create') {
                try {
                    DB::beginTransaction();
                    $title = $d['title'];
                    $content = $d['content'];
                    $authorName = $d['author_name'] ?? 'Quản trị viên';
                    $authorUrl = $d['author_url'] ?? config('app.url');
                    $category_id = $d['category_id'] ?? 0;
                    $meta_title = $d['meta_title'] ?? null;
                    $meta_keyword = $d['meta_keyword'] ?? null;
                    $meta_description = $d['meta_description'] ?? null;
                    $image = $d['image'] ?? null;
                    $comments = $d['comments'] ?? [];
                    $post_slug = str_slug($title);
                    $post = Post::where('slug', $post_slug)->first();
                    if (empty($post)) {
                        $post = new Post();
                        $post->slug = $post_slug;
                    }
                    $post->summary = $request->input('summary', '');
                    $post->category_id = $category_id;
                    $post->published = 1;
                    if ($category_id) {
                        $category = PostCategory::find($category_id);
                        if ($category) {
                            $post->category_slug = $category->slug;
                            $post->href = env('APP_URL') . '/' . $category->slug . '/' . $post_slug;
                        } else {
                            $post->category_id = 0;
                        }
                    }

                    $article = new Article();
                    $article->title = $title;
                    $article->content = $content;
                    $article->author_name = $authorName;
                    $article->author_url = $authorUrl;

                    $photo = new Photo();
                    $photo->alt = $request->input('alt', '');
                    $photo->url = $image;
                    $photo->zip_url = $image;

                    $meta = new MetaData();
                    $meta->title = $meta_title;
                    $meta->keyword = $meta_keyword;
                    $meta->description = $meta_description;

                    $post->save();
                    $post->photo()->save($photo);
                    $post->article()->save($article);
                    $post->meta()->save($meta);

                    $createComments = [];
                    if (!empty($comments)) {
                        foreach ($comments as $item) {
                            $c = new ArticleComment();
                            $c->author = $item['author_name'];
                            $c->content = $item['content'];
                            $c->parent_id = 0;
                            $c->published = $item['status'] ?? 1;
                            array_push($createComments, $c);
                        }
                        $article = $post->article;
                        $article->comments()->saveMany($createComments);
                    }

                    $post->load(['article', 'meta', 'photo', 'category', 'tags']);
                    DB::commit();
                    array_push($results, [
                        'status' => 1,
                        'data' => $post
                    ]);
                } catch (\Exception $e) {
                    Log::error($e);
                    DB::rollBack();
                    array_push($results, [
                        'status' => 0,
                        'error' => $e->getMessage()
                    ]);
                }
            } elseif ($method == 'update') {
                try {
                    DB::beginTransaction();
                    if (empty($d['id'])) {
                        throw new \Exception('Not found id in index ' . $i);
                    }
                    $post = (new Post())->where('id', $d['id'])->first();
                    if (empty($post)) {
                        throw new \Exception('Not found post ' . $d['id']);
                    }
                    $category_id = $d['category_id'] ?? 0;
                    $comments = $d['comments'] ?? [];

                    $post->image = $d['image'] ?? $post->image;
                    $post->published = $d['status'] ?? $post->published;
                    if ($category_id != $post->category_id) {
                        $category = PostCategory::find($category_id);
                        $post->category_slug = $category->category_slug;
                        $post->category_id = $category_id;
                    }


                    $article = $post->article;
                    $article->title = $d['title'] ?? $article->title;
                    $article->content = $d['content'] ?? $article->content;
                    $meta = $post->meta;
                    $meta->title = $d['meta_title'] ?? $meta->title;
                    $meta->keyword = $d['meta_keyword'] ?? $meta->keyword;
                    $meta->description = $d['meta_description'] ?? $meta->description;

                    $post->save();
                    $article->save();
                    $meta->save();

                    $createComments = [];
//                    if (!empty($comments)) {
//                        foreach ($comments as $item) {
//                            if (isset($item['id'])) {
//                                $c = Comment::find($item['id']);
//                                if (empty($c)) {
//                                    throw new \Exception('Not found comment ' . $item['id']);
//                                }
//                                $c->author_name = $item['author_name'] ?? $c->author_name;
//                                $c->content = $item['content'] ?? $c->content;
//                                $c->posted_at = $item['posted_at'] ?? $c->posted_at;
//                            } else {
//                                $c = new Comment();
//                                $c->author_name = $item['author_name'];
//                                $c->content = $item['content'];
//                                $c->posted_at = $item['posted_at'];
//                            }
//                            array_push($createComments, $c);
//                        }
//                        $post->comments()->saveMany($createComments);
//                    }
                    $post->load(['article', 'meta', 'photo', 'category', 'tags']);

                    DB::commit();
                    array_push($results, [
                        'status' => 1,
                        'data' => $post
                    ]);
                } catch (\Exception $e) {
                    Log::error($e);
                    DB::rollBack();
                    array_push($results, [
                        'status' => 0,
                        'error' => $e->getMessage()
                    ]);
                }
            } elseif ($method == 'delete') {
                try {
                    DB::beginTransaction();
                    if (empty($d['id'])) {
                        throw new \Exception('Not found id in index ' . $i);
                    }
                    $post = Post::with(['article', 'meta', 'photo', 'category', 'tags'])->find($d['id']);
                    if (empty($post)) {
                        throw new \Exception('Not found post ' . $d['id']);
                    }
//                    $post->comments()->delete();
                    $post->delete();
                    DB::commit();
                    array_push($results, [
                        'status' => 1
                    ]);
                } catch (\Exception $e) {
                    Log::error($e);
                    DB::rollBack();
                    array_push($results, [
                        'status' => 0,
                        'error' => $e->getMessage()
                    ]);
                }
            }
        }
        return $this->success($results);
    }
}
