<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\ArticleComment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index($id)
    {
        $data = ArticleComment::whereHas('article', function ($q) use ($id) {
            $q->where('articleable_type', 'posts')->where('articleable_id', $id);
        })
            ->where('parent_id', 0)
            ->with('children')->get();
        return $this->success($data);
    }

    public function store(Request $request, $id)
    {
        try {
            $post = Post::find($id);
            $article = $post->article;
            $data = new ArticleComment();
            $data->author = $request->input('author_name');
            $data->content = $request->input('content');
            $data->parent_id = $request->input('parent_id', 0);
            $data->article_id = $article->id;
            $data->published = $request->input('status', 1);
            $post->article()->save($data);
            $data->save();
            return $this->success($data);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        $data = ArticleComment::find($id);
        if (!$data) {
            return $this->error('Comment not found');
        }
        try {
            $data->content = $request->input('content');
            $data->author = $request->input('author_name');
            $data->published = $request->input('status', $data->status);
            $data->save();
            return $this->success($data);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function destroy($id, $comment_id)
    {
        $comment = ArticleComment::where('id', $comment_id)->first();
        if (!$comment) {
            return $this->error('Comment not found');
        }
        try {
            $comment->delete();
            return $this->success($comment);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function disable($id, $comment_id)
    {
        $comment = ArticleComment::where('id', $comment_id)->first();
        if (empty($comment)) {
            return $this->error('Comment not found');
        }
        $comment->published = 0;
        $comment->save();
        return $this->success($comment);
    }

    public function ArticleComment($id, $comment_id)
    {
        $comment = ArticleComment::where('id', $comment_id)->first();
        if (empty($comment)) {
            return $this->error('Comment not found');
        }
        $comment->published = 1;
        $comment->save();
        return $this->success($comment);
    }
}
