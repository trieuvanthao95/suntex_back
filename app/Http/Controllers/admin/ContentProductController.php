<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\ContentArticle;
use App\Models\ContentProduct;
use App\Models\SeoMeta;
use App\Models\Slug;
use App\Utils\AuthUtil;
use App\Utils\FileUtil;
use App\Utils\HtmlUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ContentProductController extends Controller implements ApiController
{
    public function index(Request $request)
    {
        $search = $request->input('search', null);
        $limit = $request->input('limit', null);
        $category_id = $request->input('category_id', null);
        $query = (new ContentProduct())
            ->when($search, function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%');
            })
            ->when($category_id, function ($q) use ($category_id) {
                $q->where('category_id', $category_id);
            })
            ->with('meta', 'category', 'article', 'gallery')
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC');
        if ($limit) {
            $data = $query->paginate($limit);
        } else {
            $data = $query->get();
        }
        return $this->success($data);
    }

    public function store(Request $request)
    {
        $auth = AuthUtil::getInstance()->getModel();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors());
        }
        $name = $request->input('name');
        $slug = $request->input('slug');
        if (!$slug) {
            $slug = Str::slug($name);
        }
        $check = Slug::where('slug', $slug)->first();
        if ($check) {
            return $this->error('Đường dẫn đã tồn tại');
        }

        $label = $request->input('label');
        if (ContentProduct::where('slug', $slug)->exists()) {
            return $this->error('Tên sản phẩm trùng');
        }
        $model = new ContentProduct();
        $model->slug = $slug;
        $model->name = $request->name;
        $model->summary = $request->input('summary');
        $model->published = $request->input('published', 1);
        $model->order = $request->input('order');
        $model->price = $request->input('price');
        if ($request->hasFile('image')) {
            $url = Storage::disk('public')->put('images', $request->file('image'));
            if (empty($url)) {
                return $this->error('Not Found File');
            }
            $zipUrl = FileUtil::compressImage($url, 25);
            $model->image = $url;
            $model->image_zip = $zipUrl;
            $model->alt = $request->input('alt', $model->name);
        }

        if ($request->hasFile('image2')) {
            $url2 = Storage::disk('public')->put('images', $request->file('image2'));
            if (empty($url2)) {
                return $this->error('Not Found File');
            }
            $zipUrl2 = FileUtil::compressImage($url2, 25);
            $model->image2 = $url2;
            $model->image2_zip = $zipUrl2;
            $model->alt2 = $request->input('alt2', $model->name);
        }

        $meta = new SeoMeta();
        $meta->description = $request->input('meta_description', HtmlUtil::getSummaryContent($model->summary ));
        $meta->keywords = $request->input('meta_keywords');
        $meta->robots = $request->input('meta_robots');
        $meta->canonical = $request->input('meta_canonical');
        $meta->more = $request->input('other_code');
        try {
            DB::beginTransaction();
            $model->save();
            $model->meta()->save($meta);
            $slug = new Slug();
            $slug->slug = $model->slug;
            $slug->slugable_type = 'products';
            $slug->slugable_id = $model->id;
            $slug->save();
            $category = $request->input('category');
            if ($category){
                $categories = json_decode($category);
                $category_id = [];
                foreach ($categories as $value) {
                    array_push($category_id, $value->id);
                }

                $model->category()->sync($category_id);
            }
            if ($label) {
                $labels = json_decode($label);
                $label_ids = [];
                foreach ($labels as $value) {
                    array_push($label_ids, $value->id);
                }

                $model->labels()->sync($label_ids);
            }
            DB::commit();
            $model->load('meta', 'category');
            return $this->success($model);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }
    }

    public function show($id)
    {
        $data = ContentProduct::with('category', 'labels', 'meta', 'article')->find($id);
        return $this->success($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors());
        }
        $label = $request->input('label');
        $name = $request->input('name');
        $slug = $request->input('slug');
        if (!$slug) {
            $slug = Str::slug($name);
        }

        $model = (new ContentProduct())->with('meta')->find($id);
        if ($model->slug != $slug) {
            $check = Slug::where('slug', $slug)->first();
            if ($check) {
                return $this->error('Đường đẫn đã tồn tại');
            }
            $slug_update = Slug::where('slugable_type', 'products')->where('slugable_id', $id)->first();
            $slug_update->slug = $slug;
            $slug_update->save();
            $model->slug = $slug;
        }else{
            $model->slug = $slug;
        }
        $model->name = $request->name;
        $model->category_id = 0;
        $model->weight = $request->input('weight');
        $model->summary = $request->input('summary');
        $model->published = $request->input('published');
        $model->order = $request->input('order');
        $model->price = $request->input('price');
        if ($request->hasFile('image')) {
            $url = Storage::disk('public')->put('images', $request->file('image'));
            if (empty($url)) {
                return $this->error('Not Found File');
            }
            $zipUrl = FileUtil::compressImage($url, 25);
            FileUtil::removePublicFile($model->image);
            FileUtil::removePublicFile($model->image_zip);
            $model->image = $url;
            $model->image_zip = $zipUrl;
            $model->alt = $request->input('alt', $model->name);
        }

        if ($request->hasFile('image2')) {
            $url = Storage::disk('public')->put('images', $request->file('image2'));
            if (empty($url)) {
                return $this->error('Not Found File');
            }
            $zipUrl = FileUtil::compressImage($url, 25);
            FileUtil::removePublicFile($model->image2);
            FileUtil::removePublicFile($model->image2_zip);
            $model->image2 = $url;
            $model->image2_zip = $zipUrl;
            $model->alt2 = $request->input('alt2', $model->name);
        }
        $category = $request->input('category');
        if ($category){
            $categories = json_decode($category);
            $category_id = [];
            foreach ($categories as $value){
                array_push($category_id, $value->id);
            }
            $model->category()->sync($category_id);
        }else{
            $model->category()->detach();
        }
        if ($label) {
            $labels = json_decode($label);
            $label_id = [];
            foreach ($labels as $value) {
                array_push($label_id, $value->id);
            }
            $model->labels()->sync($label_id);
        } else {
            $model->labels()->detach();
        }
        try {
            DB::beginTransaction();
            $model->save();
            $model->meta()->update(
                [
                    'keywords' => $request->input('meta_keyword'),
                    'description' => $request->input('meta_description'),
                    'canonical' => $request->input('meta_canonical'),
                    'robots' => $request->input('meta_robots')
                 ]);
            DB::commit();
            $model->load('meta', 'category');
            return $this->success($model);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }
    }

    public function destroy($id)
    {
        $model = ContentProduct::find($id);
        if (empty($model)) {
            return $this->error('Not found');
        }
        FileUtil::removePublicFile($model->image);
        FileUtil::removePublicFile($model->image_zip);
        FileUtil::removePublicFile($model->image2);
        FileUtil::removePublicFile($model->image2_zip);
        $model->meta()->delete();
        $model->article()->delete();
        $model->delete();
        return $this->success($model);
    }

    public function enable($id)
    {
        $model = ContentProduct::find($id);
        if (empty($model)) {
            return $this->error('Not found');
        }
        try {
            DB::beginTransaction();
            $model->published = true;
            $model->created_at = now()->toDateTimeString();
            $model->save();
            DB::commit();
            return $this->success($model);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }
    }

    public function disable($id)
    {
        $model = ContentProduct::find($id);
        if (empty($model)) {
            return $this->error('Not found');
        }
        try {
            DB::beginTransaction();
            $model->published = false;
            $model->save();
            DB::commit();
            return $this->success($model);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }
    }

    public function change_order(Request $request, $id)
    {
        $data = ContentProduct::find($id);
        $data->order = $request->order;
        $data->save();
        return $this->success($data);
    }

    public function delete_product(Request $request)
    {
        $item = $request->input('item');
        for ($i = 0; $i < count($item); $i++) {
            $product = ContentProduct::find($item[$i]);
            $product->labels()->detach();
            $product->meta()->delete();
            FileUtil::removePublicFile($product->image);
            FileUtil::removePublicFile($product->image_zip);
            FileUtil::removePublicFile($product->image2);
            FileUtil::removePublicFile($product->image2_zip);
            $product->delete();
        }
        return $this->success($product);
    }
}

