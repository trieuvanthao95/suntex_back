<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\ApiController;
use App\Models\ContentPhoto;
use App\Utils\FileUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ContentPhotoController extends Controller implements ApiController
{
    public function index(Request $request)
    {
        $limit = $request->input('limit', '');
        $query = new ContentPhoto();
        if ($limit) {
            $data = $query->paginate($limit);
        } else {
            $data = $query->get();
        }
        return $this->success($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photoable_type' => 'required',
            'photoable_id' => 'required',
            'url' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors());
        }

        $url = Storage::disk('public')->put('images', $request->file('url'));
        if (empty($url)) {
            return $this->error('Not Found File');
        }
        $zipUrl = FileUtil::compressImage($url, 25);
        try {
            DB::beginTransaction();
            $model = new ContentPhoto();
            $model->photoable_type = $request->input('photoable_type', '');
            $model->photoable_id = $request->input('photoable_id', 0);
            $model->alt = $request->input('alt');
            $model->image = $url;
            $model->image_zip = $zipUrl;
            $model->order = $request->input('order');
            $model->save();
            DB::commit();
            return $this->success($model);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }
    }

    public function show($id)
    {
        $model = ContentPhoto::find($id);
        if (empty($model)) {
            return $this->error('Not found');
        }
        return $this->success($model);
    }

    public function update(Request $request, $id)
    {
        $model = (new ContentPhoto())->find($id);
        $url = $model->url;
        $oldzipUrl = $model->image_zip;
        $oldFileURL = $model->url;
        if ($request->hasFile('url')) {
            $url = Storage::disk('public')->put('images', $request->file('url'));
            if (empty($url)) {
                return $this->error('Not Found File');
            }
            $zipUrl = FileUtil::compressImage($url, 25);
            FileUtil::removePublicFile($oldFileURL);
            FileUtil::removePublicFile($oldzipUrl);
        }

        try {
            DB::beginTransaction();
            $model->alt = $request->input('alt');
            $model->url = $url;
            $model->image_zip = $zipUrl ?? $model->image_zip;
            $model->order = $request->input('order');
            $model->save();
            DB::commit();
            return $this->success($model);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->error($e->getMessage());
        }

    }

    public function destroy($id)
    {
        $model = ContentPhoto::find($id);
        if (empty($model)) {
            return $this->error('Not found');
        }
        $url = $model->url;
        $image_zip = $model->image_zip;
        FileUtil::removePublicFile($image_zip);
        FileUtil::removePublicFile($url);
        $model->delete();
        return $this->success($model);
    }
}
