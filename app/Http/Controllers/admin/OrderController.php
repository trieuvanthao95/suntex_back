<?php

namespace App\Http\Controllers\admin;

use App\Models\ContentProduct;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', null);
        $status = $request->input('status', null);
        $date = $request->input('date')?Carbon::parse($request->input('date'))->timestamp : null;
        $customer_ids = $request->input('customer_ids')?explode(',', $request->input('customer_ids')) : null;
        $sql = (new Order())
            ->when($status, function ($q) use($status){
                $q->where('status', '=', $status);
            })
            ->when($date, function ($q) use($date){
                $q->where('date', $date);
            })
            ->when($customer_ids, function ($q) use($customer_ids){
                $q->whereIn('customer_id', $customer_ids);
            })
            ->with('order_details.product', 'order_details.type', 'customer')
            ->orderBy('id', 'DESC');
        if($limit){
            $data = $sql->paginate($limit);
        }else{
            $data = $sql->get();
        }
        return $this->success($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_details' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors());
        }
        $sub_total = 0;
        $customer_id = $request->input('customer_id', null);
        $order_detail = $request->input('order_details', null);
        foreach ($order_detail as $item){
            $product = ContentProduct::find($item['product_id']);
            $price = $product->price ?? 0;
            $sub_total = $sub_total + $price*$item['quantity'];
        }
        $date = Carbon::now()->toDateString();
        $date = Carbon::parse($date)->timestamp;
        $order = new Order();
        $order->customer_id = $customer_id[0]['id'];
        $order->sub_total = $sub_total;
        $order->total = $sub_total;
        $order->date = $date;
        $order->status = 0;
        $order->save();
        foreach ($order_detail as $item){
            $detail = new OrderDetail();
            $detail->product_id = $item['product_id'];
            $detail->order_id = $order->id;
            $detail->quantity = $item['quantity'];
            $product = ContentProduct::find($item['product_id']);
            $detail->sub_total = $product->price;
            $detail->total = $product['price']*$item['quantity'];
            $detail->save();
        }
        return $this->success($order->load('order_details.product', 'customer'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Order::with('order_details.product', 'customer')->find($id);
        $data->total = $request->input('total');
        $data->note = $request->input('note');
        $data->status = $request->input('status');
        $data->save();
        $data->load('order_details.product', 'customer');
        return $this->success($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Order::find($id);
        $data->order_details()->delete();
        return $this->success($data);
    }

    public function export(Request $request){
        $start_date = $request->input('start_date')?Carbon::parse($request->input('start_date'))->timestamp : null;
        $end_date = $request->input('end_date')?Carbon::parse($request->input('end_date'))->timestamp : null;
        $status = $request->input('status', null);
        $data =Order::where('date', '>=', $start_date)
            ->where('date', '<=', $end_date)
            ->when($status, function ($q) use($status){
                $q->where('status', $status);
            })
            ->with('customer.province', 'location')
            ->get();
        return $this->success($data);
    }
}
