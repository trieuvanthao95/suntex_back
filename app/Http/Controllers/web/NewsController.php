<?php

namespace App\Http\Controllers\web;

use App\Models\ContentComment;
use App\Models\ContentPage;
use App\Models\ContentPost;
use App\Models\ContentPostCategory;
use App\Utils\BreadcrumbsUtil;
use App\Utils\CacheUtil;
use App\Utils\HtmlUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index(){
        $news = (new ContentPost())
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(8);
        BreadcrumbsUtil::add(env("APP_URL").'/tin-tuc/', 'Tin tức');
        $meta = array();
        $metas = ContentPage::where('slug', 'tin-tuc')->with('meta','article')->first();
        $page_title = $metas['article']['title'];
        $page_description = $metas['article']['content'];
        if ($metas && isset($metas->meta)){
            $meta['title'] = $metas->meta->title;
            $meta['description'] = $metas->meta->description;
            $meta['keywords'] = $metas->meta->keywords;
            $meta['robot'] = $metas->meta->robots;
            $meta['canonical'] = $metas->meta->canonical;
        }
        return view('news', compact('news', 'meta','page_title','page_description'));
    }

    public static  function list($slug){
        $post_category = ContentPostCategory::where('slug', $slug)->where("published",1)->with(['meta'])->first();
        if(!$post_category){
            return abort(404);
        }
        $news = ContentPost::where('category_id', $post_category['id'])
            ->where("published",1)
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(8);
        $model = new ContentPostCategory();
        $parents = array();
        $model->get_all_parentid($parents,$post_category['id']);
        $parents = array_reverse($parents);
        if($parents){
            foreach ($parents as $parent){
                $parent_info = ContentPostCategory::where('id', $parent)->first();
                BreadcrumbsUtil::add($parent_info->full_path,$parent_info->name);
            }
        }

        BreadcrumbsUtil::add(null,$post_category['name']);

        $meta = ['title'=>'','description'=>'','keywords'=>'', 'image' => null];
        if (isset($post_category->meta)) {
            $meta['title'] = $post_category->meta->title;
            $meta['description'] = $post_category->meta->description;
            $meta['keywords'] = $post_category->meta->keywords;
        }
        if(!$meta['title']) $meta['title'] = $post_category->name;
        if(!$meta['description']) $meta['description'] = HtmlUtil::extractShortText($post_category->summary,'160','...');
        if(!$meta['keywords']) $meta['keywords'] = $post_category->name;
        if(isset($post_category->photo)){
            $meta['image'] = $post_category->photo->full_path;
        }
        $meta['canonical'] = $post_category['full_path'];
        $page_title = $post_category->name;
        if($post_category->id==55) return view('service-list', compact('news','post_category', 'meta','page_title'));
        else return view('news-list', compact('news','post_category', 'meta','page_title'));
    }

    public static function detail($slug){
        $news = ContentPost::where('published', 1)
            ->where('slug', $slug)
            ->with('meta', 'article')
            ->first();
        if (!$news) {
            return abort(404);
        }
        $model = new ContentPostCategory();
        $similar = ContentPost::where('category_id', $news['category_id'])
            ->where("id", '<', $news['id'])
            ->where("published", 1)
            ->limit(10)
            ->get();
        $page_title = 'Tin tức';
        $parents = array($news['category_id']);
        $model->get_all_parentid($parents, $news['category_id']);
        $parents = array_reverse($parents);
        if ($parents) {
            foreach ($parents as $parent) {
                $parent_info = ContentPostCategory::where('id', $parent)->first();
                if ($parent_info){
                    if($parent_info->id==$news['category_id'])    $page_title = $parent_info['name'];
                        BreadcrumbsUtil::add($parent_info->full_path, $parent_info->name);
                }
            }
        }

        BreadcrumbsUtil::add($news['full_path'], $news['name']);

        $meta = ['title' => '', 'description' => '', 'keywords' => '', 'image' => null];
        if (isset($news->meta)) {
            $meta['title'] = $news->meta->title;
            $meta['description'] = $news->meta->description;
            $meta['keywords'] = $news->meta->keywords;
        }
        if (!$meta['title']) $meta['title'] = $news->article->title;
        if (!$meta['description']) $meta['description'] = HtmlUtil::extractShortText($news->article->content, '160', '...');
        if (!$meta['keywords']) $meta['keywords'] = $news->article->title;
        if ($news['image']) {
            $meta['image'] = $news['image_full_path'];
        }

        return view('news-detail', compact('news', 'similar', 'meta','page_title'));
    }
}
