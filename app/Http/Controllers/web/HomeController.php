<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\ContentPage;
use App\Utils\CacheUtil;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $page = ContentPage::where('slug', 'trang-chu')->first();

        $meta = ['title' => '', 'description' => '', 'keywords' => '', 'image' => null, 'canonical' => null,'robots'=>null];
        $structured_datas = null;
        if($page) {
            if (isset($page->meta)) {
                $meta['title'] = $page->meta->title;
                $meta['description'] = $page->meta->description;
                $meta['keywords'] = $page->meta->keywords;
                $meta['robots'] = $page->meta->robots;
                $meta['canonical'] = $page->meta->canonical;
            }
            foreach ($page->structured_datas as $struct) {
                $structured_datas .= $struct->code;
            }
        }

        return view('index', compact('structured_datas', 'meta'));
    }

}
