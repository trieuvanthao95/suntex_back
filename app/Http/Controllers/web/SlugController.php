<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\SettingLabel;
use App\Models\Slug;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;

class SlugController extends Controller
{
    public function index($slug)
    {
        $slugs = Slug::where('slug', $slug)->first();
        if (!$slugs) {
            return abort(404);
        }

        switch ($slugs->slugable_type) {
            case 'post_categories':
                return NewsController::list($slug);
                break;
            case 'posts':
                return NewsController::detail($slug);
                break;
            case 'product_categories':
                return ProductController::list($slug);
                break;
            case 'products':
                return ProductController::detail($slug);
                break;
            case 'pages':
                return PageController::show($slug);
                break;
            case 'tags':
                break;
            default:
                return abort(404);
        }
        return abort(404);
    }
}
