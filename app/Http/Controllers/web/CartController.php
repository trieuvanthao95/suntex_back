<?php

namespace App\Http\Controllers\web;

use App\Models\ContentPage;
use App\Models\ContentProduct;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\SettingDefault;
use App\Models\SettingLanguage;
use App\Utils\BreadcrumbsUtil;
use App\Utils\CacheUtil;
use App\Utils\StringUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $content = \Cart::getContent();
        $content = $content->sort();
        $page_title = 'Giỏ hàng';
        BreadcrumbsUtil::add(env("APP_URL") . '/cart', 'Giỏ hàng');
        return view('cart', compact('content','page_title'));
    }

    public function delete_item($id)
    {
        \Cart::remove($id);
        return redirect()->to('cart');
    }

    public function down_item($id)
    {
        $get_item = \Cart::get($id);
        if ($get_item['quantity'] == 1) \Cart::remove($id);
        else \Cart::update($id, ['quantity' => -1]);
        return redirect()->to('cart');
    }

    public function up_item($id)
    {
        \Cart::update($id, ['quantity' => +1]);
        return redirect()->to('cart');
    }

    public function update_quantity($id, $quantity)
    {
        if ($id) {
            if ($quantity == 0) \Cart::remove($id);
            else {
                \Cart::update($id, ['quantity' => array(
                    'relative' => false,
                    'value' => $quantity
                )]);
            }
        }
        return redirect()->to('cart');
    }

    public function add_item(Request $request)
    {
        $product_id = $request->get('product_id');
        $data = ContentProduct::find($product_id);
        $price = $data->price ?? 0;
        if ($data) {
            $cart = \Cart::add(array(
                'id' => $data['id'],
                'name' => $data['name'],
                'price' => $price,
                'product_id' => $product_id,
                'quantity' => 10,
                'attributes' => array('full_path' => $data['full_path'],
                    'image_full_path' => $data['image_full_path'],
                    'packing' => $data['packing'],
                    'weight' => $data['weight'],
                    'type' => $request->get('type')
                )
            ));
        }
        return redirect()->to('cart');
    }
    public function add($product_id,Request $request)
    {
        $data = ContentProduct::find($product_id);
        $price = $data->price ?? 0;
        if ($data) {
            $cart = \Cart::add(array(
                'id' => $data['id'],
                'name' => $data['name'],
                'price' => $price,
                'product_id' => $product_id,
                'quantity' => 10,
                'attributes' => array('full_path' => $data['full_path'],
                    'image_full_path' => $data['image_full_path'],
                    'packing' => $data['packing'],
                    'weight' => $data['weight'],
                    'type' => $request->get('type')
                )
            ));
        }
        return redirect()->to('cart');
    }

    public function get_checkout(Request $request)
    {
        $content = \Cart::getContent();
        $page_title = 'Mua hàng';
        BreadcrumbsUtil::add(env("APP_URL") . '/cart', 'Mua hàng');
        return view('checkout', compact('content','page_title'));
    }

    public function checkout(Request $request)
    {
        $content = \Cart::getContent();
        $name = $request->input("name");
        $phone = $request->input("phone");
        $address = $request->input("address");
        $email = $request->input("email");
        $more_request = $request->input("request");
        if (count($content) && $name && $phone && $address) {
            $total_weight = 0;
            $sub_total = \Cart::getTotal();
            $total = $sub_total;

            $order = new Order();
            $order->status = 'O';
            $order->sub_total = $sub_total;
            $order->total = $total;
            $order->date = time();
            $order->request = $more_request;
            $order->customer_id = 0;
            $code = str_pad($order->id,5,0,STR_PAD_LEFT);
            $order->code = $code;
            try {
                DB::beginTransaction();
                $order->save();
                foreach ($content as $product) {
                    $order_detail = new OrderDetail();
                    $order_detail->order_id = $order->id;
                    $order_detail->product_id = $product['id'];
                    $order_detail->quantity = $product['quantity'];
                    $order_detail->sub_total = $product['price'];
                    $order_detail->total = $product['price'] * $product['quantity'];
                    $order_detail->created_at = date("Y-m-d H:i:s");
                    $order_detail->save();
                }
                $customer = new Customer();
                $customer->name = $name;
                $customer->phone = $phone;
                $customer->email = $email;
                $customer->address = $address;
                $customer->created_at = date("Y-m-d H:i:s");
                $customer->save();
                $order->customer_id = $customer->id;
                $code = str_pad($order->id,5,0,STR_PAD_LEFT);
                $order->code = $code;
                $order->update();
                DB::commit();
                $header_sales =
                    'User IP: ' . @$_SERVER['REMOTE_ADDR'] . '<br>' .
                    'Connection: ' . @$_SERVER['HTTP_CONNECTION'] . '<br>' .
                    'Via: ' . @$_SERVER['SERVER_PROTOCOL'] . '<br>' .
                    'Referer: ' . @$_SERVER['HTTP_REFERER'] . '<br>' .
                    'User-Agent: ' . @$_SERVER['HTTP_USER_AGENT'] . '<br>';

                $data = array(
                    'content' => $content,
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'more_request' => $more_request,
                    'total' => $total,
                    'sub_total' => $sub_total,
                    'order_code' => $code,
                    'date'=> 'Ngày '.date('d').' tháng '.date('m').' năm '.date("Y").' '.date("H:i:s"),
                    'http_code' => $header_sales
                );
                if (!isset($widgets)) $widgets = CacheUtil::getWidget();
                if (!isset($settings)) $settings = CacheUtil::getAppSetting();
                if (!isset($language)) $language = CacheUtil::getLanguage();
                $master_email = isset($settings['master_email'])?$settings['master_email']:env('MAIL_USERNAME');
                $master_name = isset($settings['master_name'])?$settings['master_name']:env('MAIL_FROM_NAME');
                $subject = isset($language['subject_email_confirm_order'])?$language['subject_email_confirm_order']:'subject_email_confirm_order';

                $order_info = view('mails.order_string', $data)->render();
                $email_content = '';
                $email_order = $widgets['template_email_order'];
                if(($email_order)){
                    $email_content = strtr($email_order['html'], array(
                        '[NAME]' => $name,
                        '[ORDER_CODE]'=> $code,
                        '[DATE]'=> $data['date'],
                        '[EMAIL]' => $email,
                        '[PHONE]' => $phone,
                        '[ADDRESS]' => $address,
                        '[CONTENT]' => $order_info
                    ));
                }
                $data['email_content'] = $email_content;

                \Cart::clear();
                Mail::send('mails.order', $data, function ($message) use ($code, $content, $total, $settings, $subject, $name, $email, $phone, $more_request, $address, $master_name, $master_email) {
                    $message->from(env('MAIL_USERNAME') ?? $settings['master_email'], $master_name);
                    $message->sender(env('MAIL_USERNAME') ?? $settings['master_email'], $master_name);
                    $message->to($master_email, $master_name);
                    if($email && StringUtil::validateAddress($email)) {
                        $message->replyTo($email, $name);
                    }else {
                        $message->replyTo($master_email, $name);
                    }
                    $message->subject($subject . ' #' . $code);
                    if (isset($settings['email_cc'])) {
                        $email_cc = explode(',', $settings['email_cc']);
                        if (is_array($email_cc)) {
                            foreach ($email_cc as $em) {
                                $message->cc($em);
                            }
                        }
                    }
                });

                if($email && StringUtil::validateAddress($email)) {
                    Mail::send('mails.order', $data, function ($message) use ($code, $content, $total, $settings, $subject, $name, $email, $phone, $more_request, $address, $master_name, $master_email) {
                        $message->from(env('MAIL_USERNAME') ?? $email, $master_name);
                        $message->sender(env('MAIL_USERNAME') ?? $email, $master_name);
                        $message->to($email, $name);
                        $message->replyTo($master_email, $master_name);
                        $message->subject($subject . ' #' . $code);
                    });
                }

                $message = isset($language['message_order_success'])?$language['message_order_success']:'message_order_success';

                $page = ContentPage::where('slug', 'order-success')
                    ->first();

                if (empty($page)) {
                    return view('thanks', [
                        'status' => true,
                        'message' => $message
                    ]);
                } else {
                    return redirect()->to($page->full_path);
                }
            } catch (\Exception $e) {
                Log::error($e);
                $message = isset($language['message_order_fail'])?$language['message_order_fail']:'message_order_fail';

                $page = ContentPage::where('slug', 'order-fail')
                    ->first();

                if (empty($page)) {
                    return view('thanks', [
                        'status' => true,
                        'message' => $message
                    ]);
                } else {
                    return redirect()->to($page->full_path);
                }
            }
        } else return redirect()->to('cart');

    }

}
