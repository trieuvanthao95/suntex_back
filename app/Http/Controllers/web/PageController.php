<?php

namespace App\Http\Controllers\web;

use App\Models\ContentPage;
use App\Utils\HtmlUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public static function show($slug)
    {
        $page = ContentPage::where('slug', $slug)
            ->published()
            ->with(['article', 'meta','structured_datas'])
            ->first();
        if(!$page){
            return abort(404);
        }
        $meta = ['title' => '', 'description' => '', 'keywords' => '', 'image' => null, 'url' =>null];
        if (isset($page->meta)) {
            $meta['title'] = $page->meta->title;
            $meta['description'] = $page->meta->description;
            $meta['keywords'] = $page->meta->keywords;
            $meta['robots'] = $page->meta->robots;
            $meta['canonical'] = $page->meta->canonical;
        }
        if(!$meta['title']) $meta['title'] = $page->article->title;
        if(!$meta['description']) $meta['description'] = HtmlUtil::extractShortText($page->article->content,'160','...');

        $structured_datas = null;
        foreach ($page->structured_datas as $struct) {
            $structured_datas .= $struct->code;
        }
        return view('page-detail', compact('page', 'meta','structured_datas'));
    }
}
