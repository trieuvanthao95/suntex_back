<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\FormContact;
use App\Models\ContentPage;
use App\Models\SettingDefault;
use App\Models\SettingLanguage;
use App\Models\SettingWidget;
use App\Utils\BreadcrumbsUtil;
use App\Utils\CacheUtil;
use App\Utils\StringUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function index()
    {
        $page = ContentPage::where('slug', 'lien-he')
            ->published()
            ->with(['article', 'meta'])
            ->first();
        if (!$page) {
            return abort(404);
        }
        $page_title = $page->article->title;
        $meta = [];
        if (isset($page->meta)) {
            $meta['title'] = $page->meta->title;
            $meta['description'] = $page->meta->description;
            $meta['keywords'] = $page->meta->keywords;
        }
        $meta['canonical'] = $page->full_path;
        BreadcrumbsUtil::add($page->full_path,$page_title);
        $data = array(
            '[NAME]' => '<input type="text" name="name" id="name" class="form-control" required/>',
            '[EMAIL]' => '<input type="text" name="email" id="email" class="form-control"/>',
            '[PHONE]' => '<input type="text" name="phone" id="phone" class="form-control" required/>',
            '[ADDRESS]' => '<input type="text" name="address" id="address" class="form-control">',
            '[CONTENT]' => '<textarea name="request" id="request" class="form-control" required></textarea>',
            '[BUTTON]' => '<button style="width: 150px" class="btn btn-large btn-secondary btn-checkout" type="submit"><i class="fa fa-envelope"></i> Gửi đi</button>',
        );
        $content = strtr($page->article->content,$data);
        return view('contact', compact('meta','page','content'));
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'request' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors());
        }
        try {
            $formContact = new FormContact();
            $name = $request->input('name');
            $email = $request->input('email');
            $phone = $request->input('phone');
            $address = $request->input('address');
            $content = $request->input('request');

            $formContact->name = $name;
            $formContact->email = $email;
            $formContact->phone = $phone;
            $formContact->address = $address;
            $formContact->content = $content;
            $formContact->save();

            if (!isset($settings)) $settings = SettingDefault::get_all();
            if (!isset($language)) $language = SettingLanguage::get_code();

            $data = array(
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'address' => $address,
                'content' => $content,
            );
            $master_email = isset($settings['master_email']) ? $settings['master_email'] : env("MAIL_FROM_ADDRESS");
            $master_name = isset($settings['master_name']) ? $settings['master_name'] : env("MAIL_FROM_NAME");
            $subject = isset($language['subject_email_contact']) ? $language['subject_email_contact'] : 'subject_email_contact';

            $email_content = '';
            $email_contact = SettingWidget::get_code("email_contact");
            if(($email_contact)){
                $email_content = strtr($email_contact['html'], array(
                    '[NAME]' => $name,
                    '[EMAIL]' => $email,
                    '[PHONE]' => $phone,
                    '[ADDRESS]' => $address,
                    '[CONTENT]' => $content
                ));
            }

            Mail::send('mails.contact', $data, function ($message) use ($settings,$subject, $name, $email, $phone, $address, $content, $master_name, $master_email) {
                $message->from(env('MAIL_USERNAME'), $master_name);
                $message->sender(env('MAIL_USERNAME'), $master_name);
                $message->to($master_email, $master_name);
                $message->replyTo($master_email, $name);
                $message->subject($subject.' - '.$name);
                if(isset($settings['email_cc'])){
                    $email_cc = explode(',',$settings['email_cc']);
                    if(is_array($email_cc)){
                        foreach ($email_cc as $em){
                            $message->cc($em);
                        }
                    }
                }
            });
        } catch (\Exception $e) {
            Log::error($e);
        }
        $page = ContentPage::where('slug', 'contact-message-success')
            ->published()
            ->with(['article', 'meta'])
            ->first();
        if(empty($page)){
            redirect('thanks', [
                'status' => true,
                'message' =>  'Cảm ơn bạn đã liên hệ!'
            ]);
        }else {
            return redirect($page->full_path);
        }
    }
}
