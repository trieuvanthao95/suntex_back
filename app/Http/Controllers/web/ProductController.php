<?php

namespace App\Http\Controllers\web;

use App\Models\ContentGallery;
use App\Models\ContentPage;
use App\Models\ContentProduct;
use App\Models\ContentProductCategory;
use App\Models\ProductType;
use App\Utils\BreadcrumbsUtil;
use App\Utils\HtmlUtil;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(){
        $products = ContentProduct::where('published', 1)->orderBy('order', 'DESC')->orderBy("name",'ASC')->paginate(20);
        BreadcrumbsUtil::add('/tat-ca-san-pham', 'Sản phẩm');
        $meta = array();
        $metas = ContentPage::where('slug', 'tat-ca-san-pham')->with('meta','article')->first();
        if ($metas && isset($metas->meta)){
            $meta['title'] = $metas->meta->title;
            $meta['description'] = $metas->meta->description;
            $meta['keywords'] = $metas->meta->keywords;
            $meta['robot'] = $metas->meta->robots;
            $meta['canonical'] = $metas->meta->canonical;
        }
        $page_title = 'Tất cả sản phẩm';
        return view('product', compact('products', 'meta','page_title'));
    }

    public static function list($slug){
        $categoryinfo = ContentProductCategory::where('slug', $slug)
            ->where("published", 1)
            ->with('meta')
            ->first();
        if (!$categoryinfo) {
            return abort(404);
        }
        $model = new ContentProductCategory();

        $childs = array($categoryinfo->id);
        $model->get_all_childs($childs,$categoryinfo->id);
        $childs = array_reverse($childs);

        $products = ContentProduct::where('published', 1)
            ->whereHas('category', function ($q) use($childs){
                $q->whereIn('category_id', $childs);
            })
            ->orderBy("order", 'DESC')
            ->orderBy("name",'ASC')
            ->paginate(20);

        $parents = array($categoryinfo->id);
        $model->get_all_parentid($parents,$categoryinfo->id);
        $parents = array_reverse($parents);
        if ($parents) {
            foreach ($parents as $parent) {
                $parent_info = ContentProductCategory::where('id', $parent)->first();
                if($parent_info) BreadcrumbsUtil::add($parent_info->full_path, $parent_info->name);
            }
        }

        $meta = ['title' => '', 'description' => '', 'keywords' => '', 'image' => null, 'url' =>null];
        if (isset($categoryinfo->meta)) {
            $meta['title'] = $categoryinfo->meta->title;
            $meta['description'] = $categoryinfo->meta->description;
            $meta['keywords'] = $categoryinfo->meta->keywords;
        }
        if (!$meta['title']) $meta['title'] = $categoryinfo->name;
        if (!$meta['description']) $meta['description'] = HtmlUtil::extractShortText($categoryinfo->summary, '160', '...');
        if (!$meta['keywords']) $meta['keywords'] = $categoryinfo->name;
        $meta['canonical'] = $categoryinfo->full_path;
        $meta['image'] = $categoryinfo['image_full_path'];
        $meta['url'] = $categoryinfo['full_path'];
        $page_title = $categoryinfo->name;
        return view('product-list', compact('products', 'categoryinfo', 'meta','page_title'));
    }

    public static function detail($slug){
        $product = ContentProduct::where('slug', $slug)->where("published", 1)
            ->with('meta', 'article')
            ->first();
        if (!$product) {
            return abort(404);
        }
        BreadcrumbsUtil::add(env("APP_URL").'/tat-ca-san-pham', 'Sản phẩm');

        $category = DB::table('content_product_categories')
            ->join('category_product', 'category_product.category_id', '=', 'content_product_categories.id')
            ->where('category_product.product_id','=',$product->id)
            ->select('content_product_categories.id')
            ->first();
        $category_id = 0;
        if($category){
            $category = ContentProductCategory::where([['id','=', $category->id],['published','=',1]])->first();
            if($category) {
                BreadcrumbsUtil::add($category->full_path, $category->name);
                $category_id = $category->id;
            }
        }
       if($category_id){
           $product_similar = ContentProduct::where('published', 1)
               ->whereHas('category', function ($q) use ($category_id) {
                   $q->where('category_id', $category_id);
               })
               ->orderBy("order", 'DESC')
               ->orderBy("name",'ASC')
               ->paginate(20);

       }else {
           $product_similar = ContentProduct::where('id', '<>', $product['id'])
               ->where('published', '=', 1)
               ->limit(12)
               ->get();
       }
        BreadcrumbsUtil::add(null, $product->name);
        $gallery = ContentGallery::where('galleryable_type', 'products')
            ->where('galleryable_id', $product['id'])
            ->with('images')
            ->first();
        $type = ProductType::all();
        $meta = ['title' => '', 'description' => '', 'keywords' => '', 'image' => null, 'url' => null];
        if (isset($product->meta)) {
            $meta['title'] = $product->meta->title;
            $meta['description'] = $product->meta->description;
            $meta['keywords'] = $product->meta->keywords;
        }
        if (!$meta['title']) $meta['title'] = $product->name;
        if (!$meta['description']) $meta['description'] = HtmlUtil::extractShortText($product->summary, '160', '...');
        if (!$meta['keywords']) $meta['keywords'] = $product->name;
        $meta['image'] = $product['image_full_path'];
        $meta['canonical'] = $product->full_path;
        $meta['url'] = $product['full_path'];
        $page_title = 'Sản phẩm';
        return view('product-detail', compact( 'page_title','product', 'meta', 'product_similar', 'gallery', 'type'));
    }
    public function search(Request $request)
    {
        $search = $request->input('search', null);
        $query = (new ContentProduct())
            ->where("published", 1)
            ->when($search, function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%')
                ->orWhere('summary', 'like', '%' . $search . '%');
            })
            ->orderBy('order', 'ASC')
            ->orderBy("name",'ASC');
        $products = $query->paginate(20);
        $meta = ['title' => '', 'description' => '', 'keywords' => '', 'image' => null, 'url' =>null];
        $meta['title'] = $page_title = 'Tìm kiếm sản phẩm';
        $meta['description'] = 'Tìm kiếm sản phẩm';
        return view('search', compact('products', 'search','meta','page_title'));
    }
}
