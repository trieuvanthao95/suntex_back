<?php
namespace App\Utils;
class Table_Of_Contents {
    public static function add_toc( $content = '',$save=true ) {
        require_once 'simple_html_dom.php';
        $html = str_get_html($content);
        if($content) {
            $toc = '';
            $last_level = 0;
            $heading = $html->find('h1,h2,h3,h4,h5,h6');
            if ($heading) {
                $toc .= '<div class="ez-toc-container">';
                $toc .= '<p class="ez-toc-title">Mục lục</p>';
                foreach ($heading as $h) {
                    $innerTEXT = trim(strip_tags($h->plaintext));
                    $id = Table_Of_Contents::nonSign(($innerTEXT));
                    $h->id = $id; // add id attribute so we can jump to this element
                    $level = intval($h->tag[1]);
                    if ($level > $last_level)
                        $toc .= "<ul class='ez-toc-list'>";
                    else {
                        $toc .= str_repeat('</li></ul>', $last_level - $level);
                        $toc .= '</li>';
                    }
                    $toc .= "<li><a href='#{$id}'>{$innerTEXT}</a>";
                    $last_level = $level;
                }
                $toc .= '</div>';
                $toc .= str_repeat('</li></ul>', $last_level);
                if ($save) {
                    $html_with_toc = $toc . $html->save();
                    return $html_with_toc;
                } else {
                    return array("toclist" => $toc, 'content' => $html->save());
                }
            } else if ($save) return $content;
        }
        else return;
    }
    /**
     * Add IDs and anchor links to the headings.
     *
     * @param string $tag     Tag to add anchor to.
     * @param string $content Post content.
     *
     * @return string
     */
    private static function nonSign($str,$separator='-'){

        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);

        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);

        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);

        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);

        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);

        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);

        $str = preg_replace("/(đ)/", 'd', $str);

        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);

        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);

        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);

        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);

        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);

        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);

        $str = preg_replace("/(Đ)/", 'D', $str);

        $str=  preg_replace('/[^a-zA-Z0-9_|-]/s', ' ', $str);

        $str=preg_replace('/\s\s+/', ' ',$str);

        $str=trim($str);

        $str = str_replace(" ", $separator, str_replace("&*#39;","",$str));

        return strtolower($str);

    }
    private static function add_ids_and_jumpto_links( $tag, $content ) {
        $items = self::get_tags( $tag, $content );
        $first = true;

        foreach ( $items as $item ) {
            $replacement = '';
            $matches[]   = $item[0];
            $id          = self::nonSign( $item[2],'_' );



            $replacement   .= sprintf( '<%1$s id="%2$s">%3$s </%1$s>', $tag, $id, $item[2] );
            $replacements[] = $replacement;
        }

        $content = str_replace( $matches, $replacements, $content );

        return $content;
    }

    /**
     * Get the tags from post_content
     *
     * @param string $tag     Tag to search for.
     * @param string $content Post content.
     *
     * @return array
     */
    private static function get_tags( $tag, $content = '' ) {

        preg_match_all( "/(<{$tag}>)(.*)(<\/{$tag}>)/", $content, $matches, PREG_SET_ORDER );
        return $matches;
    }
}
