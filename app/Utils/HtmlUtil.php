<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 31/05/2018
 * Time: 1:50 CH
 */

namespace App\Utils;


use App\Models\Widget;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class HtmlUtil
{
    public static function getFirstImage(string $content)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTMl(htmlspecialchars($content));
        $images = $dom->getElementsByTagName('img');
        if ($images->length > 0) {
            return $images->item(0)->getAttribute('src');
        }
        return null;
    }


    public static function minify(string $html, array &$createdImages)
    {
        $xml = '<?xml encoding="utf-8" ?>';
        $doc = new \DOMDocument();
        @$doc->loadHTML($xml . $html);

        $images = $doc->getElementsByTagName('img');
        for ($i = 0; $i < $images->length; $i++) {
            $image = $images->item($i);
            $src = $image->getAttribute('src');
            if (Str::startsWith($src, 'data:image/')) {
                $fileSavedUrl = FileUtil::base64ToImage($src);
                if (empty($fileSavedUrl)) {
                    continue;
                }
                array_push($createdImages, $fileSavedUrl);
                $url = Storage::disk('public')->url($fileSavedUrl);
                $image->setAttribute('src', $url);
            }
        }
        preg_match("#<body>([\s\S]*)</body>#", trim($doc->saveHTML()), $matches);
        return empty($matches) ? '' : $matches[1];
    }

    public static function getSummaryContent($contentHtml)
    {
        $summary = strip_tags(html_entity_decode($contentHtml));
        return mb_substr($summary, 0, 200);
    }

    public static function extractShortText($str, $length = 200, $etc = '...')
    {
        if ($str == '' || !isset($str)) return NULL;
        $str = strip_tags($str);
        if (strlen($str) <= $length) return $str;
        else {
            if (substr($str, $length, 1) != ' ') {
                $str = mb_substr($str, 0, $length, mb_detect_encoding($str));
                if ($pad = strrpos($str, ' ')) {
                    return mb_substr($str, 0, $pad, mb_detect_encoding($str)) . $etc;
                } else return $str . $etc;
            } else {
                return mb_substr($str, 0, $length, mb_detect_encoding($str)) . $etc;
            }
        }
    }

    public static function price_format($price = null)
    {
        if (!is_null($price)) {
            return  number_format($price, 0, ',', '.').'đ';
        }
    }

    public static function widget_replace(string $content)
    {
        $xml = '<?xml encoding="utf-8" ?>';
        $doc = new \DOMDocument();
        @$doc->loadHTML($xml . $content);
        $images = $doc->getElementsByTagName('div');
        $data = [];
        for ($i = 0; $i < $images->length; $i++) {
            $image = $images->item($i);
            if ($image->getAttribute('class') == 'widget') {
                $id = $image->getAttribute('id');
                $widget = Widget::find($id)->html;
                $image->nodeValue = $widget;
            }
        }
        preg_match("#<body>([\s\S]*)</body>#", trim($doc->saveHTML()), $matches);
        return empty($matches) ? '' : $matches[1];

        //Use: $post['content'] = HtmlUtil::widget_replace($post->content);
    }

    public static function formatted_content($content,$rel='group')
    {
        preg_match_all('/<img[^>]+>/i', $content, $result);
        foreach ($result[0] as $r) {
            preg_match_all('/(src)=("[^"]*")/i', $r, $img);
            $check = strpos($content, 'href=' . $img[2][0]);
            if ($check == false) {
                $content = str_replace($r, '<a class="fancybox" rel="'.$rel.'" href=' . $img[2][0] . '>' . $r . '</a>', $content);
            }
        }

        $html = $content;
        $xml = '<?xml encoding="utf-8" ?>';
        $doc = new \DOMDocument();
        @$doc->loadHTML($xml . $html);

        $images = $doc->getElementsByTagName('img');

        for ($i = 0; $i < $images->length; $i++) {
            $image = $images->item($i);
            $src = $image->getAttribute('src');
            $alt = $image->getAttribute('alt');
            $dataSrc = $src;

            $image->setAttribute('src', $dataSrc);
            $image->setAttribute('data-src', $src);
            $image->setAttribute('class', 'lazy img-responsive');
            $image->parentNode->setAttribute('data-fancybox', 'images');
        }
        preg_match("#<body>([\s\S]*)</body>#", trim($doc->saveHTML()), $matches);

        return empty($matches) ? '' : $matches[1];
    }
}
