<?php
namespace App\Utils;

class BreadcrumbsUtil
{
    static $breadcrumbs = array();

    static function add($url=null,$text=null){
        if($text) array_push(self::$breadcrumbs,array('url'=>$url,'text'=>$text));
    }

    static function getVars(){
        return self::$breadcrumbs;
    }
}
