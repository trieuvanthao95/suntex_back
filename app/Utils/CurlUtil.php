<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2018
 * Time: 9:08 AM
 */

namespace App\Utils;

use Ixudra\Curl\Facades\Curl;

class CurlUtil
{
    public static function curlGet($path, $data, $header, $option=null)
    {
        $response = Curl::to($path)
            ->withData($data)
            ->withContentType('application/json; charset=utf-8')
            ->withOption($option)
            ->withHeader('Authorization: ' . $header)
            ->withTimeout(300)
            ->asJson()
            ->returnResponseObject()
            ->get();
        return $response;
    }

    public static function curlPost($path, $body, $header, $option=null)
    {
        $response = Curl::to($path)
            ->withData($body)
            ->withContentType('application/json; charset=utf-8')
            ->withOption($option)
            ->withHeader('Authorization: ' . $header)
            ->asJson()
            ->returnResponseObject()
            ->withTimeout(300)
            ->post();
        return $response;
    }

    public static function curlPut($path, $body, $header, $option=null)
    {
        $response = Curl::to($path)
            ->withData($body)
            ->withContentType('application/json; charset=utf-8')
            ->withOption($option)
            ->withHeader('Authorization: ' . $header)
            ->asJson()
            ->returnResponseObject()
            ->withTimeout(300)
            ->put();
        return $response;
    }

    public static function curlDelete($path, $body, $header, $option=null)
    {
        $response = Curl::to($path)
            ->withData($body)
            ->withContentType('application/json; charset=utf-8')
            ->withOption($option)
            ->withHeader('Authorization: ' . $header)
            ->asJson()
            ->returnResponseObject()
            ->withTimeout(300)
            ->delete();
        return $response;
    }
}
