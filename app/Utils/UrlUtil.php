<?php

namespace App\Utils;

class UrlUtil{
    static function buildUrl($con = null, $func = null){
        if ($con == 'TourCategory'){
            if($func == 'show')return '/tours/';
        }elseif ($con == 'Tour'){
            if($func == 'show')return '/tours/';
            if($func == 'book')return 'tours';
        }elseif ($con == 'CruiseCategory'){
            if($func == 'show')return '/cruises/';
        }elseif ($con == 'Cruise'){
            if($func == 'show')return '/cruises/';
            if($func == 'index')return '/cruises/';
        }elseif ($con == 'GuideCategory'){
            if($func == 'show')return '/guides/';
        }elseif ($con == 'GuideCategory') {
            if ($func == 'show') return '/guides/';
        }elseif ($con == 'Faq'){
            if($func == 'index') return '/faq.html';
        }
        else{
            return null;
        }
    }
}
